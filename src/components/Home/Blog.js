import React from 'react';
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import { Container } from 'react-bootstrap';
import '../../css/Blog.css';


// const image1 = [{ src: ('url("../images/ybeegru.png")') }, { src: '../../images/2.png' }, { src: '../../images/1.png' }];

export default class Blog extends React.Component {
  render() {

    return (
      <Container id="Blog">


        <CardDeck>

          <Card className="text-center p-3">
            <Card.Img className="blog1" />
            <Card.ImgOverlay>
              <blockquote className="blockquote mb-0 card-body">
                <small>
                  DESTINATIONS
        </small>
                <p>
                  Affordable neighborhoods for teachers
      </p>
              </blockquote>
            </Card.ImgOverlay>
          </Card>

          <Card bg="secondary" text="white" className="text-center p-3">
            <blockquote className="blockquote mb-0 card-body">
              <small>
                DESTINATIONS
        </small>
              <p>
                Affordable neighborhoods for teachers
      </p>
            </blockquote>
          </Card>
          <Card bg="warning" text="white" className="text-center p-3">
            <blockquote className="blockquote mb-0 card-body">
              <small>
                DESTINATIONS
        </small>
              <p>
                Affordable neighborhoods for teachers
      </p>
            </blockquote>
          </Card>





        </CardDeck>


      </Container>

    );
  }
}