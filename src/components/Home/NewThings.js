import React, { Component } from 'react'
import { Row, Col, Container } from 'react-bootstrap';
import { RegisterasAgent, PostyourRequirement, ListyourProperty, ListyourServices } from './../Register'

export default class NewThings extends Component {
        constructor(...args) {
                super(...args);

                this.state = { modalShow: false };
        }
        render() {
                let modalClose = () =>
                        this.setState({

                                modalShow3: false,
                                modalShow4: false,
                                modalShow5: false,
                                modalShow6: false
                        });
                return (
                        <div id="newThings">
                                <div className="newThingsSubHeading">
                                        Things you can do
                                </div>
                                <div className="newThingsHeading">
                                        Get Started
                                </div>
                                <Container className="newThingsItemsContainer">


                                        <Col className="newThingsItemWrapper col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                                <Row className="newThingsItem " onClick={() => this.setState({ modalShow5: true })}>
                                                        <Col className="newThingsItemImage listYourProperty col-lg-12 col-md-12 col-sm-3 col-xs-3 ">
                                                                <div className="plusCircle"><div className="plus"></div></div>
                                                                {/* <span className="plus"> <i className="fa fa-plus-circle" aria-hidden="true"></i> </span> */}
                                                        </Col>
                                                        <Col className="newThingsItemHeadText col-lg-12 col-md-12 col-sm-9 col-xs-9 ">List Your Property</Col>
                                                </Row>
                                        </Col>
                                        <ListyourProperty show={this.state.modalShow5} onHide={modalClose} />

                                        <Col className="newThingsItemWrapper col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                                <Row className="newThingsItem" onClick={() => this.setState({ modalShow6: true })}>
                                                        <Col className="newThingsItemImage postYourReqd col-lg-12 col-md-12 col-sm-3 col-xs-3 ">
                                                                <div className="homeUpper"><div className="homeDown"></div></div>
                                                                {/* <span className="home"> <i className="fa fa-home" aria-hidden="true"></i> </span> */}
                                                        </Col>
                                                        <Col className="newThingsItemHeadText col-lg-12 col-md-12 col-sm-9 col-xs-9">Post Your Requirement</Col>
                                                </Row>
                                        </Col>
                                        <PostyourRequirement
                                                show={this.state.modalShow6}
                                                onHide={modalClose}
                                        />

                                        <Col className="newThingsItemWrapper col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                                <Row className="newThingsItem" onClick={() => this.setState({ modalShow3: true })}>
                                                        <Col className="newThingsItemImage becomeAnAgent col-lg-12 col-md-12 col-sm-3 col-xs-3 ">
                                                                <div className="rupeeContainer"><div className="rupee"></div></div>
                                                                {/* <span className="rupee"> <i className="fa fa-inr" aria-hidden="true"></i> </span> */}
                                                        </Col>
                                                        <Col className="newThingsItemHeadText col-lg-12 col-md-12 col-sm-9 col-xs-9 ">Become An Agent</Col>
                                                </Row>
                                        </Col>
                                        <RegisterasAgent show={this.state.modalShow3} onHide={modalClose} />



                                        <Col className="newThingsItemWrapper col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                                <Row className="newThingsItem" onClick={() => this.setState({ modalShow4: true })}>
                                                        <Col className="newThingsItemImage listYourService col-lg-12 col-md-12 col-sm-3 col-xs-3 ">
                                                                {/* <div className="serviceUpper"><div className="serviceDown"></div></div> */}
                                                                {/* <span className="home"> <i className="fa fa-home" aria-hidden="true"></i> </span> */}
                                                        </Col>
                                                        <Col className="newThingsItemHeadText col-lg-12 col-md-12 col-sm-9 col-xs-9 ">List Your Services</Col>
                                                </Row>
                                        </Col>
                                        <ListyourServices
                                                show={this.state.modalShow4}
                                                onHide={modalClose}
                                        />


                                </Container>

                        </div>
                )
        }
}
