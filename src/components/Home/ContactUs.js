import React, { Component } from 'react'
import { Container, Row, Col, Form } from 'react-bootstrap';

export default class ContactUs extends Component {
        render() {
                return (

                        <div >
                                <Container id="contactUs">
                                        <h6>Contact Us</h6>
                                        <Row>
                                                <Col md="8">
                                                        <Form>
                                                                <Form.Row>
                                                                        <Form.Group as={Col} controlId="formFullName">
                                                                                <Form.Label className="textBoxHeading">FULL NAME *</Form.Label>
                                                                                <Form.Control className="headingTextBox" type="text" placeholder="Full Name" />
                                                                        </Form.Group>
                                                                        <Form.Group as={Col} controlId="formEmail">
                                                                                <Form.Label className="textBoxHeading">EMAIL *</Form.Label>
                                                                                <Form.Control className="headingTextBox" type="email" placeholder="Email" />
                                                                        </Form.Group>

                                                                </Form.Row>
                                                                <Form.Row>
                                                                        <Form.Group as={Col} controlId="formPhoneNumber">
                                                                                <Form.Label className="textBoxHeading">PHONE NUMBER *</Form.Label>
                                                                                <Form.Control className="headingTextBox" type="text" placeholder="Enter Phone Number" />

                                                                        </Form.Group>

                                                                        <Form.Group as={Col} controlId="formLocation">
                                                                                <Form.Label className="textBoxHeading">LOCATION *</Form.Label>
                                                                                <Form.Control className="headingTextBox" type="text" placeholder="Location" />
                                                                        </Form.Group>

                                                                </Form.Row>
                                                                <Form.Row>
                                                                        <Form.Group as={Col} controlId="formMessage">

                                                                                <Form.Label className="textBoxHeading">MESSAGE</Form.Label>
                                                                                <Form.Control className="headingTextBox" type="textarea" as="textarea" placeholder="Message" />

                                                                        </Form.Group>
                                                                        <Form.Group as={Col} controlId="formMessage">

                                                                        </Form.Group>
                                                                </Form.Row>

                                                                <div className="button submitText">Submit</div>
                                                        </Form>
                                                </Col>
                                                <Col md={1}>
                                                        <div className="divider">

                                                        </div>
                                                </Col>
                                                <Col md={3}>
                                                        <div className="submitFormImage">

                                                        </div>
                                                </Col>
                                        </Row>
                                </Container>


                        </div>

                )
        }
}
