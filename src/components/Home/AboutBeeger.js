import React, { Component } from 'react';
import { Row, Col, Container, Button } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import About from "./../about";
import imgfamily from "./../../images/familyPaintingWallsTogether.png"
import { Player } from 'video-react';

export default class AboutBeeger extends Component {

        render() {
                return (
                        // <div id="aboutBeeger">
                        //         <div className="familyPaintingWall">
                        //                 <div className="centered">
                        //                         <i className='fab fa-youtube' style={{ color: '#BD0000' }} ></i>
                        //                 </div>
                        //         </div>
                        //         <div className="aboutBeegerInfo">
                        //                 <h1 className="aboutBeegerHeading">About Beegru</h1>
                        //                 <div className="aboutBeegerText">Beegru is a marketing advisory firm which connect owners of
                        //                  property with potential buyers, Lessors, Investors or Joint Venture partners. Beegru wants
                        //                   to put forth the properties which it markets in the most honest and
                        //                 reasonable way which will simplify the decision-making process for the potential client.</div>
                        //                 <Link to="/about">
                        //                         <div className="knowMoreButton" variant="secondary">Know More</div></Link>
                        //                 <Route path="/about" component={About} />
                        //         </div>
                        // </div>
                        <div id="aboutBeegru">
                                <Container>
                                        <h1 className="aboutBeegruHeading mr-auto">About Beegru</h1>
                                        <Row>

                                                <Col md={4}>
                                                        <div className="aboutBeegruText">
                                                                Beegru is a marketing advisory firm which connect owners of
                                                          property with potential buyers, Lessors, Investors or Joint Venture partners. Beegru wants
                                                         to put forth the properties which it markets in the most honest and
                                                         reasonable way which will simplify the decision-making process for the potential client.
                                                        </div>
                                                        <div className="knowMoreSection">
                                                                <Link to="/about">
                                                                        <Button className="knowMoreButton" variant="secondary">Know More</Button></Link>
                                                                <Route path="/about" component={About} />
                                                        </div>
                                                </Col>

                                                <Col md={{ span: 4, offset: 4 }} >

                                                        <Player
                                                                playsInline
                                                                poster={imgfamily}
                                                                src=""
                                                        />

                                                </Col>

                                        </Row>

                                </Container>
                        </div>
                )
        }
}
