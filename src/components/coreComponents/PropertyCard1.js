import React from 'react';
import Card from 'react-bootstrap/Card';
import "../../css/cardConatiner.css";
import image from "../../images/property.png";
// import PropertyPage from "../PropertyPage/PropertyPage";
import {  Row, Col } from 'react-bootstrap';

export default class PropertyCard extends React.Component {
        constructor(props) {
                super(props);
                this.cardData = this.props.cardData ? this.props.cardData : {};
                this.generatedData = undefined;
                this.onClickHandler = this.onClickHandler.bind(this);

        }

        onClickHandler = cardData => {
                // <Router>
                //<Link to="/PropertyPage"> </Link>
                // </Router>
                //return <PropertyPage cardData={this.cardData} />
                // let cardData = this.cardData;
                this.props.onClickHandler(this.cardData);
        }
        render() {
                let propertyDetails = this.cardData ? this.cardData.propertyDetails[0] : {};
                return (

                        // <Row className="cardBottom">
                        //         <Col lg={6} md={6} sm={12} xs={12}>

                        <div className="propertyCard " id={this.cardData.id} onClick={(cardData) => this.onClickHandler(this.props.cardData)}>
                                {/* <img style={{display:"none"}} src={img}></img> */}
                                <Card className="h-100">
                                        <div className="top-left"> <i className="far fa-heart"></i> </div>
                                        <Card.Img variant="top" resizemode="cover" className="card-img-top" src={/*this.cardData.images[0].mobile*/image}>

                                        </Card.Img>
                                        {/* <div><img src={image}/></div> */}
                                        <Card.Body className="propertyCardData">
                                                <Card.Title>
                                                        <h4>
                                                                <i className="fas fa-rupee-sign"></i> {propertyDetails.priceRange}
                                                        </h4>
                                                </Card.Title>
                                                <Card.Text>
                                                        <h1 className="propertyHeading">{propertyDetails.heading}</h1>

                                                        <p className="space">
                                                                <i className="fas fa-map-marker-alt"></i>
                                                                {propertyDetails.locality + propertyDetails.distict + propertyDetails.state}
                                                        </p>
                                                        <p className="space hr">
                                                                <Row>
                                                                        <Col>
                                                                                <i className="fas fa-home"></i> TOLET / FOR SALE
                                                                        </Col>
                                                                        <Col>
                                                                                <i className="far fa-star"></i> Top 5
                                                                        </Col>
                                                                        <Col>
                                                                                <i className="fas fa-hand-holding-usd"></i> Invest
                                                                        </Col>


                                                                </Row>
                                                        </p>



                                                </Card.Text>
                                                <Row className="hr">
                                                        <Col>
                                                                <i className="far fa-object-ungroup"></i>   2400-3200 Sqft
                                                                </Col>
                                                        <Col>
                                                                <i className="far fa-building"></i>   2-5 BHK
                                                                </Col>

                                                </Row>
                                        </Card.Body>

                                </Card>


                        </div>

                        // </Col>
                        // </Row>

                )

        }
};
// PropertyCard.propTypes = {
//         data: PropTypes.object
// };
