import React from 'react';
// import Card from 'react-bootstrap/Card';
import "../../css/NewPropertyCard.css";
import image from "../../images/property1.png";
// import PropertyPage from  "../PropertyPage/PropertyPage";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Col } from 'react-bootstrap';
import { UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';

// import ReactCardCarousel from "react-card-carousel";

export default class NewPropertyCard extends React.Component {
        constructor(props) {
                super(props);
                this.cardData = this.props.cardData ? this.props.cardData : {};
                this.generatedData = undefined;
                this.onClickHandler = this.onClickHandler.bind(this);

        }
        onClickHandler = cardData => {

                // <Router>
                //<Link to="/PropertyPage"> </Link>
                // </Router>
                //return <PropertyPage cardData={this.cardData} />
                // let cardData = this.cardData;
                this.props.onClickHandler(this.cardData);
        }
        render() {
                let propertyDetails = this.cardData ? this.cardData.propertyDetails[0] : {};
                return (

                        // <Row className="cardBottom">
                        //         <Col lg={6} md={6} sm={12} xs={12}>
                        // <div className="propertyCard cardBottom" id={this.cardData.id} onClick = {(cardData) =>  this.onClickHandler(this.props.cardData)}>
                        //         {/* <img style={{display:"none"}} src={img}></img> */}
                        //                 <Card>
                        //                 <div className="top-left"> <i className="far fa-heart"></i></div>
                        //                         <Card.Img variant="top" className={"propertyCardImg"} src={/*this.cardData.images[0].mobile*/image}>

                        //                         </Card.Img>
                        //                        <Card.Body className="propertyCardData">
                        //                                 <Card.Title>
                        //                                         <h4>
                        //                                                <i className="fas fa-rupee-sign"></i>{propertyDetails.priceRange}
                        //                                         </h4>
                        //                                 </Card.Title>
                        //                                 <Card.Text>
                        //                                         <div className="propertyHeading">{propertyDetails.heading}</div>
                        //                                         <p className="space"> <i className="fas fa-building"></i>Type: Independent 5BHK</p>
                        //                                         <p className="space"><i className="fas fa-map-marker-alt"></i>{propertyDetails.locality + propertyDetails.state } <span className="house">HOUSE: TOLET / FOR SALE</span></p>



                        //                                 </Card.Text>
                        //                         </Card.Body> 

                        //                 </Card>


                        // </div>

                        <Col lg={6} md={6} sm={12} xs={12}>

                                <div className="propertyCard cardBottom" id={this.cardData.id} onClick={(cardData) => this.onClickHandler(this.props.cardData)}></div>
                                <div className="Newcard">
                                        <div className={"propertyCardImg"} src={/*this.cardData.images[0].mobile*/image}></div>
                                        <div className="pxc-stopper">
                                                <div className="propertyHeading">{propertyDetails.heading}
                                                        <p ><i className="fas fa-map-marker-alt"></i> {propertyDetails.locality + propertyDetails.distict + propertyDetails.state}
                                                                <span className="house">HOUSE: TOLET / FOR SALE</span></p>

                                                </div>
                                        </div>

                                        <div className="pxc-subcard">
                                                <div className="bhr">
                                                        <button className="salebutton">SALE</button>
                                                        <button className="rentbutton">RENT</button>
                                                        <button className="investbutton">INVEST</button>
                                                </div>

                                                <p className="Newcard-text" > <i className="fas fa-building"></i> 2-5 BHK</p>
                                                <p className="Newcard-text" ><i className="fas fa-rupee-sign"></i>{propertyDetails.priceRange}</p>
                                        </div>
                                        {/* <div id="buttonheart"> <i className="far fa-heart"></i></div>  */}
                                        <div id="buttonshare">
                                                <i className="fas fa-share-alt" data-toggle="tooltip" data-placement="top" title="I like to Share"></i>

                                        </div>
                                        <UncontrolledPopover trigger="legacy" placement="bottom" target="buttonshare">
                                                {/* <PopoverHeader>Legacy Trigger</PopoverHeader> */}
                                                <PopoverBody>
                                                        <div className="popicon">
                                                                <Link><i className='fab fa-facebook-f'></i></Link>

                                                                <Link > <i className='fab fa-twitter' ></i></Link>

                                                                <Link ><i className='fab fa-instagram' ></i></Link>

                                                                <Link ><i className='fab fa-whatsapp' ></i></Link>
                                                        </div>
                                                </PopoverBody>
                                        </UncontrolledPopover>

                                </div>

                        </Col>



                        // </Col>
                        // </Row>

                )

        }
};
// PropertyCard.propTypes = {
//         data: PropTypes.object
// };
//





