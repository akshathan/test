import React from 'react';
import SimilarPropertyCard from "../coreComponents/SimilarPropertyCard";
// import PropertyPage from  "../PropertyPage/PropertyPage";
import { Container } from 'react-bootstrap';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 3,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 2,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

export default class SimilarCardDeckContainer extends React.Component {
        constructor(props){
                super(props);     
                this.getCardData  = this.getCardData.bind(this) ;
                this.onClickHandler  = this.onClickHandler.bind(this);
        }
        onClickHandler = cardData => {
                this.props.onClickHandler(cardData);
               
        }
        getCardData(){
                let length = this.props && this.props.cardData ? this.props.cardData.length : 0;
                if(length > 0){
                        //this.setState({data : cardData});
                   this.generatedData =    this.props.cardData.map((card) => {
                        return <SimilarPropertyCard onClickHandler= {() => this.onClickHandler()} cardData = {card} /> 
                   });
                   return this.generatedData;
                }
        }
        render() {
                return (
                <Container>

                <Carousel responsive={responsive}  autoPlay={true}>
                        <div> {this.getCardData()}</div>
                        <div> {this.getCardData()}</div>
                        <div> {this.getCardData()}</div>
                        <div> {this.getCardData()}</div>
                        <div> {this.getCardData()}</div>
                        <div> {this.getCardData()}</div>
                </Carousel>
                </Container>     
                )
        }
};

