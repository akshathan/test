import React from 'react';
import PropertyCard from "../coreComponents/PropertyCard";
// import PropertyPage from  "../PropertyPage/PropertyPage";
import CardDeck from 'react-bootstrap/CardDeck';
import { Row, Col, Container, Button,Card  } from 'react-bootstrap';
// import Card from 'react-bootstrap/Card';
import image from "../../images/property.png";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export default class CardDeckContainer extends React.Component {
        constructor(props) {
                super(props);
                this.getCardData = this.getCardData.bind(this);
                this.onClickHandler = this.onClickHandler.bind(this);
                this.cardData = this.props.cardData ? this.props.cardData : {};
                this.generatedData = undefined;
                this.onClickHandler = this.onClickHandler.bind(this);
                this.state = { users: [] };
                this.viewAll = this.props.viewAll ? this.props.viewAll : "No more data";
                this.loadPropertyData = this.loadPropertyData.bind(this);
                this.state = { limit: 4};
        }


        onLoadMore = () => {
                this.setState({limit: this.state.limit + 4});
              }

        componentDidMount() {
                fetch('http://192.168.100.100:8081/website/fetchIndexInfo')
                        .then(response => {
                                return response.json();
                        }).then(result => {
                                console.log("sunil:result", result);
                                this.setState({
                                        users: result
                                });

                        });

        }

        loadPropertyData() {
                let that = this;
                let propertyData = [];
                console.log("this.state.users.featuredPropertiesList", that.state.users)
                if (that.state.users && that.state.users.featuredPropertiesList && that.state.users.featuredPropertiesList.length) {
                        
                        propertyData = that.state.users && that.state.users.featuredPropertiesList.slice(0, this.state.limit).map((proper, index) => {
                                console.log("sunil::proper", proper, index);
                                return (
                                        //        <div>
                                        //            <div className="propertyContainer">
                                        //            <img className="imageContainer"></img>
                                        //            <h2>{proper.propertyName}</h2>
                                        //            <h3>{proper.propertyType}</h3>
                                        //            <h4>{proper.propertyPrice}</h4>                   
                                        //            <h5>{proper.propertyPerSqFt}</h5>
                                        //            <p>{proper.propertyAddress}</p>
                                        //            <p>{proper.name}</p>
                                        //            <p>{proper.heading}</p>
                                        //            <p>{proper.synopsis}</p>
                                        //            <p>{proper.priceRange}</p>
                                        //            <p>{proper.minPrice}</p>
                                        //            <p>{proper.maxPrice}</p>
                                        //            <p>{proper.currency}</p>
                                        //            <p>{proper.type}</p>
                                        //            <p>{proper.location}</p>
                                        //            <p>{proper.locality}</p>
                                        //            <p>{proper.distict}</p>
                                        //            <p>{proper.propertyAction}</p>
                                        //            <p>{proper.pricePerSqft}</p>




                                        //            </div>
                                        //        </div>
                                        <div className="propertyCard " id={this.cardData.id} onClick={(cardData) => this.onClickHandler(this.props.cardData)}>
                                                {/* <img style={{display:"none"}} src={img}></img> */}
                                                <Link to="./../PropertyPage/PropertyPage" >
                                                <Card className="h-100">
                                                        <div className="top-left"> <i className="far fa-heart"></i> </div>
                                                        <Card.Img variant="top" resizemode="cover" className="card-img-top" src={/*this.cardData.images[0].mobile*/image}>

                                                        </Card.Img>
                                                        {/* <div><img src={image}/></div> */}
                                                        <Card.Body className="propertyCardData">
                                                                <Card.Title>
                                                                        <h4>
                                                                                <i className="fas fa-rupee-sign"></i> {proper.priceRange}
                                                                        </h4>
                                                                </Card.Title>
                                                                <Card.Text>
                                                                        <h1 className="propertyHeading">{proper.heading}</h1>

                                                                        <p className="space">
                                                                                <i className="fas fa-map-marker-alt"></i>
                                                                                {proper.locality + proper.distict + proper.state}
                                                                        </p>
                                                                        <p className="space hr">
                                                                                <Row>
                                                                                        <Col>
                                                                                                <i className="fas fa-home"></i> TOLET / FOR SALE
                                                                        </Col>
                                                                                        <Col>
                                                                                                <i className="far fa-star"></i> Top 5
                                                                        </Col>
                                                                                        <Col>
                                                                                                <i className="fas fa-hand-holding-usd"></i> Invest
                                                                        </Col>


                                                                                </Row>
                                                                        </p>



                                                                </Card.Text>
                                                                <Row className="hr">
                                                                        <Col>
                                                                                <i className="far fa-object-ungroup"></i>   2400-3200 Sqft
                                                                </Col>
                                                                        <Col>
                                                                                <i className="far fa-building"></i>   2-5 BHK
                                                                </Col>

                                                                </Row>
                                                        </Card.Body>

                                                </Card>
                                               </Link>

                                        </div>
                                )
                        });
                }
                console.log("")
                return propertyData;
        }

        onClickHandler = cardData => {
                this.props.onClickHandler(cardData);

        }

        getCardData() {
                let length = this.props && this.props.cardData ? this.props.cardData.length : 0;
                if (length > 0) {
                        // this.setState({data : cardData});
                        this.generatedData = this.props.cardData.map((card) => {
                                return <PropertyCard onClickHandler={() => this.onClickHandler()} cardData={card} />
                        });
                        return this.generatedData;
                }
        }
        render() {
                let featuredpropertyData = this.loadPropertyData();
                let style = this.props.style ? this.props.style : undefined;
                return (
                        <Container className="propertyCardContainer">
                                <CardDeck>
                                        <div id="cardContainer" >
                                                {/* <PropertyCard cardData={this.props.cardData} onClickHandler={this.props.clickHandler}></PropertyCard>
                                 */}
                                                {featuredpropertyData}
                                                {/* {this.getCardData()} */}
                                                {/* <PropertyCard  /> */}
                                        </div>
                                </CardDeck>
                                <div className="actionButtonContainer">
                                        <Button className="seemorebtn" type="submit" style={{ style }} onClick={this.onLoadMore}>
                                                ({this.props.buttonText ? this.props.buttonText : "See More Featured Properties"}) ({this.viewAll})
                                        </Button>
                                </div>
                        </Container>
                );
        }
}


