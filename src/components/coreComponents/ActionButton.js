import React, { Component } from 'react';
import { Button } from 'react-bootstrap';


export default class ActionButton extends Component {
        constructor(props){
                super(props);
                this.viewAll =  this.props.viewAll ? this.props.viewAll : "No more data";
        
        }
        render() {
                let style = this.props.style ? this.props.style : undefined;
                return (
                        <div className="actionButtonContainer">
                            <Button className="seemorebtn"  type="submit" style = {{ style}}>
                                ({ this.props.buttonText ? this.props.buttonText :"See More Featured Properties"}) ({this.viewAll}) 
                            </Button>
                        </div>
                )
        }
}
