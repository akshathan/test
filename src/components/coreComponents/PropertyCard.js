import React from 'react';
import Card from 'react-bootstrap/Card';
import "../../css/cardConatiner.css";
import image from "../../images/property.png";
// import PropertyPage from "../PropertyPage/PropertyPage";
import {  Row, Col } from 'react-bootstrap';

export default class PropertyCard extends React.Component {
        constructor(props) {
                super(props);
                this.cardData = this.props.cardData ? this.props.cardData : {};
                this.generatedData = undefined;
                this.onClickHandler = this.onClickHandler.bind(this);
                this.state = {users: []};
        
                this.loadPropertyData = this.loadPropertyData.bind(this);

        }

        componentDidMount() {
		fetch('https://www.aceimagingandvideo.com/beegru/login/career')
			.then(response => {
				return response.json();
			}).then(result => {
                console.log("sunil:result", result);
				this.setState({
					users:result
                });               
           
            });
            
        }

        loadPropertyData(){
                let that = this;
                let propertyData = [];
                console.log("this.state.users.propertyDetails",that.state.users)
                if( that.state.users &&  that.state.users.propertyDetails && that.state.users.propertyDetails.length ){
                    propertyData = that.state.users && that.state.users.propertyDetails.map((proper, index) => {
                    console.log("sunil::proper", proper, index);
                   return (
                //        <div>
                //            <div className="propertyContainer">
                //            <img className="imageContainer"></img>
                //            <h2>{proper.propertyName}</h2>
                //            <h3>{proper.propertyType}</h3>
                //            <h4>{proper.propertyPrice}</h4>                   
                //            <h5>{proper.propertyPerSqFt}</h5>
                //            <p>{proper.propertyAddress}</p>
                //            <p>{proper.name}</p>
                //            <p>{proper.heading}</p>
                //            <p>{proper.synopsis}</p>
                //            <p>{proper.priceRange}</p>
                //            <p>{proper.minPrice}</p>
                //            <p>{proper.maxPrice}</p>
                //            <p>{proper.currency}</p>
                //            <p>{proper.type}</p>
                //            <p>{proper.location}</p>
                //            <p>{proper.locality}</p>
                //            <p>{proper.distict}</p>
                //            <p>{proper.propertyAction}</p>
                //            <p>{proper.pricePerSqft}</p>
                          
                           
                           
                           
                //            </div>
                //        </div>
                <div className="propertyCard " id={this.cardData.id} onClick={(cardData) => this.onClickHandler(this.props.cardData)}>
                                {/* <img style={{display:"none"}} src={img}></img> */}
                                <Card className="h-100">
                                        <div className="top-left"> <i className="far fa-heart"></i> </div>
                                        <Card.Img variant="top" resizemode="cover" className="card-img-top" src={/*this.cardData.images[0].mobile*/image}>

                                        </Card.Img>
                                        {/* <div><img src={image}/></div> */}
                                        <Card.Body className="propertyCardData">
                                                <Card.Title>
                                                        <h4>
                                                                <i className="fas fa-rupee-sign"></i> {proper.priceRange}
                                                        </h4>
                                                </Card.Title>
                                                <Card.Text>
                                                        <h1 className="propertyHeading">{proper.heading}</h1>

                                                        <p className="space">
                                                                <i className="fas fa-map-marker-alt"></i>
                                                                {proper.locality + proper.distict + proper.state}
                                                        </p>
                                                        <p className="space hr">
                                                                <Row>
                                                                        <Col>
                                                                                <i className="fas fa-home"></i> TOLET / FOR SALE
                                                                        </Col>
                                                                        <Col>
                                                                                <i className="far fa-star"></i> Top 5
                                                                        </Col>
                                                                        <Col>
                                                                                <i className="fas fa-hand-holding-usd"></i> Invest
                                                                        </Col>


                                                                </Row>
                                                        </p>



                                                </Card.Text>
                                                <Row className="hr">
                                                        <Col>
                                                                <i className="far fa-object-ungroup"></i>   2400-3200 Sqft
                                                                </Col>
                                                        <Col>
                                                                <i className="far fa-building"></i>   2-5 BHK
                                                                </Col>

                                                </Row>
                                        </Card.Body>

                                </Card>


                        </div>
                   )
                });
            }
            console.log("")
            return propertyData;
            }


        onClickHandler = cardData => {
                // <Router>
                //<Link to="/PropertyPage"> </Link>
                // </Router>
                //return <PropertyPage cardData={this.cardData} />
                // let cardData = this.cardData;
                this.props.onClickHandler(this.cardData);
        }
        render() {
                
                // let propertyDetails = this.cardData ? this.cardData.propertyDetails[0] : {};
                // return (

                //         // <Row className="cardBottom">
                //         //         <Col lg={6} md={6} sm={12} xs={12}>

                        

                //         // </Col>
                //         // </Row>

                // )
                let a = this.loadPropertyData();
        return (
            <div>
               {a} 
            </div>
        )

        }
};
// PropertyCard.propTypes = {
//         data: PropTypes.object
// };