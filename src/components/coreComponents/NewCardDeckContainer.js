import React from 'react';
import NewPropertyCard from "../coreComponents/NewPropertyCard";
// import PropertyPage from  "../PropertyPage/PropertyPage";
import { Container } from 'react-bootstrap';


export default class NewCardDeckContainer extends React.Component {
        constructor(props){
                super(props);     
                this.getCardData  = this.getCardData.bind(this) ;
                this.onClickHandler  = this.onClickHandler.bind(this);
        }
        onClickHandler = cardData => {
                this.props.onClickHandler(cardData);
               
        }
        getCardData(){
                let length = this.props && this.props.cardData ? this.props.cardData.length : 0;
                if(length > 0){
                        //this.setState({data : cardData});
                   this.generatedData =    this.props.cardData.map((card) => {
                        return <NewPropertyCard onClickHandler= {() => this.onClickHandler()} cardData = {card} /> 
                   });
                   return this.generatedData;
                }
        }
        render() {
                return (
                        <Container className="propertyCardContainer">
                              
                        <div id="cardContainer" >
                                 {/* <PropertyCard cardData={this.props.cardData} onClickHandler={this.props.clickHandler}></PropertyCard>
                                 */}
                                {this.getCardData()}
                        </div>
                       
                        </Container>
                )
        }
};

