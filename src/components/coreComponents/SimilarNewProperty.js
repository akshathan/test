import React from "react";
import { Container, Row, Col, Button } from 'reactstrap';
import "bootstrap/dist/css/bootstrap.css";
import Card from 'react-bootstrap/Card';
import CardDeck from 'react-bootstrap/CardDeck';
import Modal from 'react-bootstrap/Modal';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import "../../css/NewPropertyCard.css";
import image from "../../images/property1.png";

export default class SimilarNewProperty extends React.Component {
  constructor(props) {
    super(props);
    this.cardData = this.props.cardData ? this.props.cardData : {};
    this.generatedData = undefined;
    this.onClickHandler = this.onClickHandler.bind(this);

  }
  onClickHandler = cardDataa => {

    // <Router>
    //<Link to="/PropertyPage"> </Link>
    // </Router>
    //return <PropertyPage cardData={this.cardData} />
    // let cardData = this.cardData;
    this.props.onClickHandler(this.cardData);
  }
  state = {}
  render() {
    let propertyDetails = this.cardData ? this.cardData.propertyDetails[0] : {};
    const responsive = {
      superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 5,
      },
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 1.5,
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 1,
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
      },
    };
    return (


      <Row>

        <Carousel responsive={responsive}>


          <div className="propertyCard cardBottom" id={this.cardData.id} onClick={(cardData) => this.onClickHandler(this.props.cardData)}></div>
          <Card className="Newcard">
            <div className={"propertyCardImg"} src={/*this.cardData.images[0].mobile*/image}></div>
            <div className="pxc-stopper">
              <div className="propertyHeading">{propertyDetails.heading}
                <p ><i className="fas fa-map-marker-alt"></i> {propertyDetails.locality + propertyDetails.distict + propertyDetails.state}
                  <span className="house">HOUSE: TOLET / FOR SALE</span></p>
                <div id="buttonheart"> <i className="far fa-heart"></i></div>
              </div>
            </div>

            <div className="pxc-subcard">
              <div classname="bhr">
                <button className="salebutton">SALE</button>
                <button className="rentbutton">RENT</button>
                <button className="investbutton">INVEST</button>
              </div>

              <p className="Newcard-text" > <i className="fas fa-building"></i> 2-5 BHK</p>
              <p className="Newcard-text" ><i className="fas fa-rupee-sign"></i>{propertyDetails.priceRange}</p>
            </div>

            <div id="buttonshare">
              <i className="fas fa-share-alt" data-toggle="tooltip" data-placement="top" title="I like it"></i>

            </div>

          </Card>



        </Carousel>

      </Row>

    );
  }
}





