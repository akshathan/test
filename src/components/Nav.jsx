import React, { Component } from "react";
import ScrollToTop from "react-scroll-up";
import "../css/Navbar.css";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Navbar, Nav, NavItem } from "react-bootstrap";
import Home from "./home.jsx";
import Bulletin from "./Bulletin.jsx";
import Services from "./services";
import About from "./about.jsx";
import Header from "./Header.jsx";
import Beegrufooter from "./Footer.jsx";
import Loginmodal from "./Register.jsx";
import PropertyPage from "../components/PropertyPage/PropertyPage";
import PropertyList from "../components/PropertyPage/PropertyListPage";
import logo from "../images/logo.png"; // Tell Webpack this JS file uses this image


export default class BeegruNavbar extends Component {
  state = {};
  render() {
    return (
      <Router>
        <Header />
        <Beegrunav />

        <ScrollToTop showUnder={160}>
          <span>
            <i className="fas fa-arrow-up myBtn"></i>
          </span>
        </ScrollToTop>

        <Beegrufooter />
      </Router>
    );
  }
}

class Beegrunav extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
  }

  render() {
    let modalClose = () =>
      this.setState({
        modalShow17: false
      });
  return (
    <Router>
      <Navbar collapseOnSelect expand="lg">
  <Navbar.Brand href="#home"><img src={logo} alt="Logo" /></Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link to="/home">Homepage</Nav.Link>
      <Nav.Link to="/about">About</Nav.Link>
      <Nav.Link to="/propertyPage">Property</Nav.Link>
      <Nav.Link to="/propertyList">Property List</Nav.Link>
      <Nav.Link to="/Services">SERVICES</Nav.Link>
      <Nav.Link to="/bulletin">bulletin</Nav.Link>
    </Nav>
    <Nav>
      
      <Nav.Link  to="" onClick={() => this.setState({ modalShow17: true })}>LOGIN</Nav.Link>
      <Loginmodal show={this.state.modalShow17} onHide={modalClose} />
    </Nav>
  </Navbar.Collapse>
</Navbar>

      <Route exact path="/" component={Home} />
      <Route exact path="/home" component={Home} />
      <Route path="/about" component={About} />
      <Route path="/propertyPage" component={PropertyPage} />
      <Route path="/propertyList" component={PropertyList} />

      <Route path="/services" component={Services} />
      <Route path="/bulletin" component={Bulletin} />
      
    </Router>
  );
}
}