import React, { Component } from 'react'
import { BrowserRouter as Router } from "react-router-dom";
import AboutUs from './Home/AboutUs.js';


class About extends Component {
  state = {  }
  render() { 
    return ( 
      <Router>
      <div>
        <h3>
          <AboutUs />
        </h3>
      </div>
      </Router>
     );
  }
}
 
export default About;
