import React, { Component } from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { Row, Col, Container } from "react-bootstrap";
import "../css/Footer.css";
import footerlogo from "../images/logo.png";

class Beegrufooter extends Component {
  render() {
    return (
      <div>
        <Beegrufoot />
      </div>
    );
  }
}
export default Beegrufooter;

function Beegrufoot() {
  return (
    <Router>
      <footer>
        <div className="container-fluid">
          <Row className="nomargin">
            <Col className="m35 mobile" md={4}>
              <img src={footerlogo}></img>
            </Col>

            <Col className="m35 mobile" md={4}>
              <Row>
                <Col md={6}>
                  <ul className="footerlist nomargin">
                    <li>
                      <Link to="/">Contact us</Link>
                    </li>
                    <li>
                      <Link to="/">About Beegru</Link>
                    </li>
                    <li>
                      <Link to="/">Careers</Link>
                    </li>
                    <li>
                      <Link to="/">Team Beegru</Link>
                    </li>
                    <li>
                      <Link to="/">FAQs</Link>
                    </li>
                    <li>
                      <Link to="/">EMI Calculator</Link>
                    </li>
                    <li>
                      <Link to="/">Login</Link>
                    </li>
                  </ul>
                </Col>
                <Col md={6}>
                  <ul className="footerlist nomargin">
                    <li>
                      <Link  to="/">Post requirement</Link>
                    </li>
                    <li>
                      <Link to="/">List your Property</Link>
                    </li>
                    <li>
                      <Link to="/">Register as agent</Link>
                    </li>
                    <li>
                      <Link to="/">List Your Services</Link>
                    </li>
                    <li>
                      <Link to="/">Search</Link>
                    </li>
                    <li>
                      <Link to="/">Blog</Link>
                    </li>
                  </ul>
                </Col>
              </Row>
            </Col>
            <Col className="m35" md={4}>
              <div className="ChatBtn">
                <i className="fas fa-comment-alt"></i>
              </div>
              {/* <div className="contactdiv">
                <div className="contactheading">Contact us</div>
                <div className="contactnumber">011 993 3859</div>
                <div className="contactemail">info.beegru.com</div>
                <div className="conatcticon"> 
                <i className='fab fa-whatsapp' ></i>Watsapp
                </div>
             </div> */}
            </Col>
          </Row>
        </div>
        <Container className=" hr">
          <Row>
            <Col lg={6} md={6} sm={6} xs={6} className="footercopyright">
              © Copyrights Beegru 2020
            </Col>
            <Col lg={6} md={6} sm={6} xs={6} className="footericons">
              <i className="fa fa-share-alt"></i>

              <i className="fab fa-youtube"></i>

              <i className="fab fa-facebook-f"></i>

              <i className="fab fa-twitter"></i>

              <i className="fab fa-instagram"></i>
            </Col>
          </Row>
        </Container>
      </footer>
    </Router>
  );
}
