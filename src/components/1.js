import React, { Component } from 'react';
import '../css/Navbar.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ScrollToTop from "react-scroll-up";
import Home from './home.jsx';
import Services from "./services";
import Bulletin from './Bulletin.jsx';
import About from './about.jsx';
import Header from './Header.jsx';
import Beegrufooter from './Footer.jsx';
import Register from './Register.jsx';
import PropertyPage from '../components/PropertyPage/PropertyPage';
import PropertyList from '../components/PropertyPage/PropertyListPage';
import logo from '../images/logo.png'; // Tell Webpack this JS file uses this image
import { Row, Col, Image } from 'react-bootstrap';
import { Navbar, Nav, NavItem } from "react-bootstrap";


class BeegruNavbar extends Component {
  state = {}
  render() {
    return (
      <Router>
        <Header />
        <Beegrunav />

        <ScrollToTop showUnder={160}>
          <span>
            <i className="fas fa-arrow-up myBtn"></i>
          </span>
        </ScrollToTop>

        <Beegrufooter />
      </Router>
    );
  }
}

export default BeegruNavbar;


function Beegrunav() {
  return (


    <Router>
      <Navbar fluid collapseOnSelect className="navbar navbar-expand-lg navbar-inverse ">
        {/* className="navbar navbar-inverse navbar-expand-lg" */}

        {/* <button className="navbar-toggler pull-left" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                              <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="navbar-brand">
                              <img src={logo} alt="Logo" />
                            </div> */}
        <Navbar.Header>
          <Navbar.Brand>  <img src={logo} alt="Logo" /></Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>

        {/* <div className="collapse navbar-collapse " id="navbarTogglerDemo01">
                                
                                <ul className="navbar-nav"> */}
        <Navbar.Collapse>
          <Nav pullRight></Nav>
          <NavItem eventKey={1} className=" navElement">
            {/* <li className="nav-item navElement"> */}
            <Link to="/home">Homepage </Link>
            {/* </li> */}
          </NavItem>
          <NavItem eventKey={2} className=" navElement">
            {/* <li className="nav-item navElement"> */}
            <Link to="/about">About Us </Link>
            {/* </li> */}
          </NavItem>
          <NavItem eventKey={2} className=" navElement">
            {/* <li className="nav-item navElement"> */}
            <Link to="/propertyPage" >Property</Link>
            {/* </li> */}
          </NavItem>
          <NavItem eventKey={2} className=" navElement">
            {/* <li className="nav-item navElement"> */}
            <Link to="/propertyList" >PropertyList</Link>
            {/* </li>
                                  
                                  <li className="nav-item navElement"> */}
          </NavItem>
          <NavItem eventKey={2} className=" navElement">
            {/* <div className="dropdown">
                                    <button className="dropbtn">
                                      {/* <Link to="/services">SERVICES</Link>  */}
            <Link to="/services">SERVICES</Link>
          </NavItem>
          {/* <NavItem eventKey={2} className=" navElement"></NavItem>
                                    </button>
                                    <div className="dropdown-content">
                                       <Row>
                                         <Col>
                                         <Image></Image>
                                         </Col>
                                         <Col lg={8}>
                                          <Image></Image>
                                           <h2>Tanants</h2>
                                           <span>Rent without a deposit</span>
                                         </Col>
                                       </Row>
                                    </div>
                                  </div> 
                                    
                                  </li> 
                                   <li className="nav-item navElement"> 
                                    <Link to="/bulletin">bulletin</Link>
                                  </li>
                                  <li className="nav-item navElement"><Link to="/Register">LOGIN</Link></li>
                              </ul>
                            </div>

                            <div>

                            <ul class=" login nav navbar-nav navbar-right pull-right">
                             
                            </ul>
                            </div>*/}
        </Navbar.Collapse>
      </Navbar>

      <Route exact path="/" component={Home} />
      <Route exact path="/home" component={Home} />
      <Route path="/about" component={About} />
      <Route path="/propertyPage" component={PropertyPage} />
      <Route path="/propertyList" component={PropertyList} />

      <Route path="/services" component={Services} />
      <Route path="/bulletin" component={Bulletin} />
      <Route path="/register" component={Register} />


  



    </Router>

  );
}

<ButtonToolbar>
      <ButtonGroup>
        <Button><Glyphicon glyph="align-left" /></Button>
        <Button><Glyphicon glyph="align-center" /></Button>
        <Button><Glyphicon glyph="align-right" /></Button>
        <Button><Glyphicon glyph="align-justify" /></Button>
      </ButtonGroup>
    </ButtonToolbar>