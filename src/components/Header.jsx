
// Header.js
import React, {Component} from 'react';
import '../css/Header.css';
import { BrowserRouter as Router } from "react-router-dom";
import { Row,Col}  from 'react-bootstrap';

export default class Header extends Component {
    render(){
        return (

            <Router>
            <header className="main-header">
              
                   <Row className="nav "> 
                   <Col lg={6}>
                   <Row>
                    <Col lg={5} md={5} sm={5} xs={5}>
                        <MailAddressButoon />
                    </Col>
                    <Col lg={4} md={4} sm={4} xs={4}>
                      <CallusTodayButton />
                    </Col>
                    <Col lg={3} md={3} sm={3} xs={3}>
                       <WatsappButton />
                    </Col>
                    
                    
                    </Row>

                   </Col>

                   <Col lg={6}>

                   
                    <i className='fab fa-facebook-f' ></i>
            
                   <i className='fab fa-twitter' ></i>
            
                   <i className='fab fa-instagram' ></i>
                   
                   <i className='fab fa-pinterest-p' ></i>

                   </Col>
                   

                   </Row>
                  
            </header>
            </Router>
        )
    }
}

function WatsappButton() {
    return (
        <div>
            <div className='headertext'>
                <i className='fab fa-whatsapp' ></i>Whatsapp
            </div>
        </div>
      
    );
  }

  function CallusTodayButton() {
    return (
        <div>
            <div className='headertext'>
                <i className='fas fa-phone' ></i>
                (080)234-456-7890
            </div>
        </div>
    );
  }

  function MailAddressButoon() {
    return (
        <div>
            <div className='headertext'>
                <i className='far fa-envelope' ></i>
               info@beegru.com
            </div>
        </div>
    );
  }
