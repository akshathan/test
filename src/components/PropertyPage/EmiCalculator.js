import React from "react";
import { Container, Row, Col, Button } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import '../../css/emi.css';
// import Gallery from "react-photo-gallery";
// import Carousel, { Modal, ModalGateway } from "react-images";
// import { photos } from "./photos";
// import "bootstrap/dist/css/bootstrap.css";

class Emicalc extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      totalamount: '',
      intrest: '',
      downpayment: '',
      term: '',
      emi: ''

    };
  }
  myTotalAmountChangeHandler = (event) => {
    this.setState({ totalamount: event.target.value });
  }
  myIntrestChangeHandler = (event) => {
    this.setState({ intrest: event.target.value });
  }
  myDownPaymentChangeHandler = (event) => {
    this.setState({ downpayment: event.target.value });
  }
  myTermChangeHandler = (event) => {
    this.setState({ term: event.target.value });
  }

  render() {
    return (
      <Container className="emiContainer">
        <div className="emiheading">
          <h1 className="emiText">EMI (Pay in Monthly installments)</h1>
        </div>

        <Form className="formborder">

          <Row className="form-group">
            <Col md="3">
              <label>Total Amount </label>
            </Col>
            <Col md="6">
              <div className="inputshow">Rs. {this.state.totalamount}</div>
              <input className="form-control-range inputrange" type="range" onChange={this.myTotalAmountChangeHandler} min="10000" max="2000000" step="1000" id="formControlRange1" />
            </Col>
          </Row>

          <Row className="form-group">
            <Col md="3">
              <label >Interest</label>
            </Col>
            <Col md="6">
              <div className="inputshow">{this.state.intrest}%</div>
              <input type="range" onChange={this.myIntrestChangeHandler} className="form-control-range inputrange" min="0%" max="50%" step="0.05" id="formControlRange2"></input>
            </Col>
          </Row>
          <Row className="form-group">
            <Col md="3">
              <label >DownPayment </label>
            </Col>
            <Col md="6">
              <div className="inputshow">Rs. {this.state.downpayment}</div>
              <input type="range" onChange={this.myDownPaymentChangeHandler} className="form-control-range inputrange" min="5000" max="100000" step="" id="formControlRange3" />
            </Col>
          </Row>
          <Row className="form-group">
            <Col md="3">
              <label>Term</label>
            </Col>
            <Col md="6">
              <div className="inputshow">Rs. {this.state.term}</div>
              <input type="range" onChange={this.myTermChangeHandler} className="form-control-range inputrange" min="0.6" max="20yrs" step="0.6" id="formControlRange4"></input>
            </Col>
          </Row>
          <Row className="form-group">
            <Col md="3">
              <label>EMI</label>
            </Col>
            <Col md="6">

              <input type="text"></input>

            </Col>
          </Row>
        </Form>

        {/* Enquiry form  start*/}
        <Enquire />
        {/* Enquiry form end */}


      </Container>



    );
  }
}

export default Emicalc;

function Enquire() {

  return (
    <div>
      <h1 className="enquireheading">Enquire</h1>



      <Form className="enquirecontainer">

        <Form.Group className="form-group">
          <Row>
            <Col md="3">
              <Form.Label>Name </Form.Label>
            </Col>
            <Col md="6">
              <Form.Control type="text" placeholder=" Name" className="" />
            </Col>
          </Row>
        </Form.Group>
        <Form.Group className="form-group">
          <Row>
            <Col md="3">
              <Form.Label>Phone </Form.Label>
            </Col>
            <Col md="6">
              <Form.Control type="text" placeholder="phone number" className="" />
            </Col>
          </Row>
        </Form.Group>
        <Form.Group className="form-group">
          <Row>
            <Col md="3">
              <Form.Label>Email </Form.Label>
            </Col>
            <Col md="6">
              <Form.Control type="email" placeholder="name@example.com" className="" />
            </Col>
          </Row>
        </Form.Group>
        <Form.Group className="form-group">
          <Row>
            <Col md="3">
              <Form.Label>Message </Form.Label>
            </Col>
            <Col md="6">
              <Form.Control as="textarea" placeholder="type here" rows="3" className="" />
            </Col>
          </Row>
        </Form.Group>
        <Form.Group as={Row}>
          <Col sm={{ span: 10, offset: 4 }}>
          <Button type="submit" className="btn btn-danger center" size="md" >
            Submit
          </Button>
          </Col>
        </Form.Group>
      
      </Form>
    </div >
  );
}





// function PhotoGallery() {
//     const [currentImage, setCurrentImage] = useState(0);
//     const [viewerIsOpen, setViewerIsOpen] = useState(false);

//     const openLightbox = useCallback((event, { photo, index }) => {
//       setCurrentImage(index);
//       setViewerIsOpen(true);
//     }, []);

//     const closeLightbox = () => {
//       setCurrentImage(0);
//       setViewerIsOpen(false);
//     };

//     return (
//       <div>
//         <Gallery photos={photos} onClick={openLightbox} />
//         <ModalGateway>
//           {viewerIsOpen ? (
//             <Modal onClose={closeLightbox}>
//               <Carousel
//                 currentIndex={currentImage}
//                 views={photos.map(x => ({
//                   ...x,
//                   srcset: x.srcSet,
//                   caption: x.title
//                 }))}
//               />
//             </Modal>
//           ) : null}
//         </ModalGateway>
//       </div>
//     );
//   }

