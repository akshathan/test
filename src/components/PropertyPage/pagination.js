import React from 'react';
import Pagination from 'react-bootstrap/Pagination';

export default class PropertyPagination extends React.Component {
  render() {
    return (
     
        <Pagination>
          <Pagination.First />
          <Pagination.Prev />
          <Pagination.Item active >{1}</Pagination.Item>
          <Pagination.Ellipsis />

          <Pagination.Item>{10}</Pagination.Item>
          <Pagination.Item>{11}</Pagination.Item>


          <Pagination.Ellipsis />
          <Pagination.Item>{20}</Pagination.Item>
          <Pagination.Next />
          <Pagination.Last />
        </Pagination>


        
          
          
         


         
          
    );
  }
}