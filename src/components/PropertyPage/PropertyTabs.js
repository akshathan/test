import React, { Component } from 'react';
import {ProgressBar} from 'react-bootstrap';
import '../../css/propertyTabs.css';


export default class PropertyTabs extends Component {
        constructor(props){
                super(props);
                this.beegruScore = this.beegruScore.bind(this);        
                //this.aboutProject = this.aboutProject.bind(this);        
                this.aboutLocation = this.aboutLocation.bind(this);  
                this.createUlLiListSttucture = this.createUlLiListSttucture.bind(this);
                this.loadData = this.loadData.bind(this);
                this.aboutProjectData = undefined; 
                this.state =[{
                        aboutProjectData : undefined,
                        navBar : undefined
                }]     
                this.data = this.props.data;
        }
       
        loadData(){
                let that = this;
                let data = this.data.propertyTabs;
                let propTabs = this.data.propertyTabsKey;
                this.state.navBar = propTabs.map((elem) => {
                return <li className="nav-item">
                        <a className="nav-link" href="#" tabindex="-1">{elem}</a>
                      </li>
                });
                this.setState({loadData : data.map((elem) => {
                 
                  switch(elem.id){
                    case 'aboutProject':
                    console.log("Gaurav:::about Proj",elem);
                    const aboutProjectListItems = elem.aboutProject.map((value) =>{
                      console.log("Gaurav:::aboutProject Value",value);
                     return <li>{"- " + value.value}</li>
                    });
                      return (
                        <div className="aboutProjectUlWrapper">
                              <h5 className="headingLine">{elem.text}</h5>
                        <ul className="aboutProjectUl">
                           {aboutProjectListItems} 
                        </ul>
                        </div>
                      );
                    break;
                    case 'aboutLocation':
                        console.log("Gaurav:::data Abt Loc",elem);
                    let aboutLocationListItems = elem.aboutLocation.map((value) =>{
                      console.log("Gaurav:::aboutLocation Value",value);
                      return <li>{"- " + value.value}</li>
                    });
                      return (
                        <div className="aboutLocationUlWrapper">
                              <h5>{elem.text}</h5>
                        <ul className="aboutLocationUl">
                          {aboutLocationListItems}
                        </ul>
                        </div>
                      );
                    break;
                    case 'beegruScore':
                        console.log("Gaurav:::data Beegru Scr",elem);
                        let beegruScoreListItems = elem.beegruScore.map((value) =>{
                          console.log("Gaurav:::beegruScore Value",value);
                          return <li className="beegruScore2Col">

                                  <div className="liWrapper">
                                      <div className= "progressBarKey">
                                        {value.key}
                                      </div>
                                      <div className= "progressBarUpdate">
                                        <ProgressBar now={value.value} />
                                      </div>
                                    </div>


                                  </li>
                        });
                          return (
                            <div className="beegruScoreUlWrapper">
                              <h5>{elem.text}</h5>
                            <ul className="beegruScoreUl">
                              {beegruScoreListItems}
                            </ul>
                            </div>
                          );
                    break;
                    case 'propertAmenities':
                        console.log("Gaurav:::data Prop Ame",elem);
                        let propertAmenitiesListItems = elem.propertAmenities.map((value) =>{
                          let unicode = value.unicode,
                          classNameVal = value.className;
                          console.log("Gaurav:::propertAmenities Value",value);
                          return <li>
                                  <div className="liWrapper">
                                      <div className= "unicode" style= {{fontFamily: "FontAwesome","content":unicode}}>
                                      <i className={classNameVal}>
                                        {/* {"&#x" + unicode} */}
                                      </i>
                                      </div>
                                      <div className= "unicodeText">
                                        {value.value}
                                      </div>
                                    </div>
                                  </li>
                        });
                          return (
                            <div className="propertAmenitiesWrapper">
                              <h5>{elem.text}</h5>
                            <ul className="propertAmenities">
                              {propertAmenitiesListItems}
                            </ul>
                            </div>
                          );
                    break;
                    case 'propertLocation':
                        console.log("Gaurav:::data Prop Loc",elem);
                        return <div className= "propLocation">
                          <div className="locationTextWrapper">
                              <div>Property location</div>
                              <div>Kesav Nagar, Bangalore</div>
                          </div>
                          <div className="buttonPropLocation">
                              View in Map
                          </div>
                        </div>
                    break;
                    case 'propertVicinity':
                        console.log("Gaurav:::data Proj Vic",elem);
                        let propertVicinityListItems = elem.propertVicinity.map((value) =>{
                          let id = value.id,
                          nearBy = value.nearBy;
                            let nearByVal = nearBy.map((nearBy) =>{
                              return <div className="vicinityRowWrapper">
                                {nearBy.name}<span>&nbsp;&nbsp;&nbsp;</span>{nearBy.distanceFrom}
                              </div>
                            });
                          console.log("Gaurav:::propertVicinity Value",value);
                          return <li>
                                  <div className="liWrapper">
                                      <div className= "propertVicinityId"  >
                                        {id}
                                      </div>
                                      <div className= "vicinityWrapper">
                                        {nearByVal}
                                      </div>
                                    </div>
                                  </li>
                        });
                          return (
                            <div className="propertVicinityWrapper">
                              <h5>{elem.text}</h5>
                            <ul className="propertVicinity">
                              {propertVicinityListItems}
                            </ul>
                            </div>
                          );

                    break;
                  }
                  // if(elem.id == ''){ 
                  //         console.log("Gaurav:::elem",elem.aboutProject);
                  //        //this.setState({aboutProjectData :  that.createUlLiListSttucture(elem.aboutProject)});
                  //       // console.log("GAurav:::abc",abc);
                  //       <React.Fragment key={item.id}>
                  //       <h2>{item.name}</h2>
                  //       <p>{item.url}</p>
                  //       <p>{item.description}</p>
                  //     </React.Fragment>

                  // }else   if(elem.id == ''){

                  // }else   if(elem.id == ''){

                  // }else  if(elem.id == ''){
                  
                  // }else  if(elem.id == ''){
                  
                  // }else  if(elem.id == ''){
                  
                  // }

          // });
          // return (
          //         <React.Fragment>
          //           {this.data.propertyTabs.map(item => (
                     
          //           ))}
          //         </React.Fragment>
          //       )
      })
      }); 
        }
        createUlLiListSttucture(elem){
                if(elem.length){
                const listItems = elem.map((el) =>
                        <li>{el.value}</li>
                        );
                        return (
                         <ul>{listItems}</ul>
                        );
                }
        }
        aboutLocation(){

        }
        beegruScore(){

        }
        componentDidMount(){
            let that = this;      
            this.loadData();
        }
        render() {
                console.log("Gaurav:::render");
                return (
                        <div id="propertyTabs">
                          <nav className="navbar navbar-expand-lg navbar-light bg-light">
                            
                            <div className="collapse navbar-collapse" id="navbarNav">
                              <ul className="navbar-nav propertyNavTabs">
                                {this.state.navBar}
                              </ul>
                            </div>
                          </nav>
                            {this.state.loadData}
                           
                        </div>
                );
        }
}
