import React from 'react';
import PropertyTabs from "./PropertyTabs";
import EmiCalculator from "./EmiCalculator";
import '../../css/emi.css';
// import ImageGallery from "../Gallery/ImageGallery";
import { Container} from 'react-bootstrap';
import NewProperties from "../Home/NewProperties";
import PropertyImage from "../PropertyPage/PropertyImage";

export default class PropertyPage extends React.Component {
        constructor(props){
                super(props);
                this.data = {
                        "id":"property1001",
                        "propertyData":{ 
                                
                        },
                        "images":[
                          {
                          "mobile":"/static/media/mobile.jpg",
                          "tablet":"../images/homewebsite.png",
                          "laptop":"/static/media/mobile.jpg",
                          "hd":"/static/media/mobile.jpg"
                         }
                        ],
                        "propertyTabsKey":["About","Amenities","Map","Vicinity","Calculate Emi"],
                        "propertyTabs":[
                          {
                            "text":"ABOUT Project",
                            "id":"aboutProject",
                            "aboutProject":[
                              {
                                "value":"BBP Limits"
                              },{
                                "value":"OC and CC obtained"
                              },{
                                "value":"Budget friendly ( Starting 47L ) "
                              },{
                                "value":"14 flats sold and lively resident community"
                              },{
                                "value":"24/7 security and surveillance"
                              },{
                                "value":"24/7 water supply"
                              }
                            ]
                          },
                          {
                            "text":"About Location",
                            "id":"aboutLocation",
                            "aboutLocation":[
                              {
                                "value":"0.5 km from Wipro Main gate Sarjapur Road"
                              },{
                                "value":"Near all softeare tech parks"
                              },{
                                "value":"Building viewable from an entire neighbourhood"
                              },{
                                "value":"Walkable distance to supermarket, schools, fine dining restaurant, breweries, boutiques, and malls"
                              },{
                                "value":"Away from bypass road so less traffic and less pollution"
                              },{
                                "value":"Walkable distance to the heart of evergrowing sarjapur road"
                              }
                            ]
                          },
                          {
                            "text":"Beegru Score",
                            "id":"beegruScore",
                            "beegruScore":[
                              {
                                "key":"Legal and Bank Support",
                                "value":"80"
                              },
                              {
                                "key":"Approval stage",
                                "value":"60"
                              },
                              {
                                "key":"Accesibility",
                                "value":"80"
                              },
                              {
                                "key":"Vaastu & Location",
                                "value":"80"
                              },
                              {
                                "key":"Price Value",
                                "value":"50"
                              },
                              {
                                "key":"Neighbourhood",
                                "value":"90"
                              },
                              {
                                "key":"Appreciation Potential",
                                "value":"80"
                              }
                            ]
                          },
                          {
                            "text":"Amenities",
                            "id":"propertAmenities",
                            "propertAmenities":[
                              {
                                "key":"ac",
                                "unicode":"\\f863;",
                                "value":"AC",
                                "className":"fas fa-fan"
                              },{
                                "key":"gym",
                                "unicode":"\\f44b;",
                                "value":"GYM",
                                "className":"fas fa-dumbbell"
                              },{
                                "key":"laundry",
                                "unicode":"f898;",
                                "value":"Laundry",
                                "className":"fas fa-washer"
                              },{
                                "key":"cctv",
                                "unicode":"f030;",
                                "value":"CCTV",
                                "className":"fas fa-camera"
                              },{
                                "key":"security",
                                "unicode":"f2f7;",
                                "value":"SECURITY",
                                "className":"fas fa-shield-check"
                              },{
                                "key":"wifi",
                                "unicode":"f1eb",
                                "value":"WIFI",
                                "className":"fas fa-wifi"
                              },{
                                "key":"pool",
                                "unicode":"f5c4",
                                "value":"POOL",
                                "className":"fas fa-swimmer"
                              }
                
                            ]
                          },
                          {
                            "text":"MAP",
                            "id":"propertLocation",
                            "propertLocation":[
                              {
                                "address":"Kesav Nagar, Bangalore",
                                "latitude":"",
                                "longitude":""
                              }
                            ]
                          },
                          {
                            "text":"VICINITY",
                            "id":"propertVicinity",
                            "propertVicinity":[
                              {
                                "id":"Schools",
                                "nearBy":[
                                {
                                  "name":"OldField Primary School",
                                  "distanceFrom":"0.6 miles"
                                },
                                {
                                  "name":"Guilden Sutton Cofe primary schools",
                                  "distanceFrom":"0.7 miles"
                                },
                                {
                                  "name":"The Hammold School",
                                  "distanceFrom":"0.8 miles"
                                },
                                {
                                  "name":"The bishop Blue Coat High School",
                                  "distanceFrom":"1.6 miles"
                                }
                              ]
                              },
                              {
                                "id":"Restaurant",
                                "nearBy":[
                                {
                                  "name":"OldField Primary School",
                                  "distanceFrom":"0.6 miles"
                                },
                                {
                                  "name":"Guilden Sutton Cofe primary schools",
                                  "distanceFrom":"0.7 miles"
                                },
                                {
                                  "name":"The Hammold School",
                                  "distanceFrom":"0.8 miles"
                                },
                                {
                                  "name":"The bishop Blue Coat High School",
                                  "distanceFrom":"1.6 miles"
                                }
                              ]
                              },
                              {
                                "id":"Malls",
                                "nearBy":[
                                {
                                  "name":"OldField Primary School",
                                  "distanceFrom":"0.6 miles"
                                },
                                {
                                  "name":"Guilden Sutton Cofe primary schools",
                                  "distanceFrom":"0.7 miles"
                                },
                                {
                                  "name":"The Hammold School",
                                  "distanceFrom":"0.8 miles"
                                },
                                {
                                  "name":"The bishop Blue Coat High School",
                                  "distanceFrom":"1.6 miles"
                                }
                              ]
                              },
                              {
                                "id":"Hospitals",
                                "nearBy":[
                                {
                                  "name":"OldField Primary School",
                                  "distanceFrom":"0.6 miles"
                                },
                                {
                                  "name":"Guilden Sutton Cofe primary schools",
                                  "distanceFrom":"0.7 miles"
                                },
                                {
                                  "name":"The Hammold School",
                                  "distanceFrom":"0.8 miles"
                                },
                                {
                                  "name":"The bishop Blue Coat High School",
                                  "distanceFrom":"1.6 miles"
                                }
                              ]
                              }
                            ]
                          },
                          {
                            "text":"CALCULATE EMI",
                            "id":"calculateEmi"
                          }
                        ], 
                        "propertyDetails":[
                          {
                                "propertyName":"Jaz Aquaviva Hurghada",
                                "propertyAddess":"Bangalore",
                                "propertyType":"House Tolet/For Sale",
                                "propertyPrice":"53,88,678",
                                "propertyPerSqFt":"5757\/sqft",
                                "propertyPriceIcons":[{
                                        "icon":"upload",
                                        "key":"upload",
                                        "value":"upload"
                                },
                                {
                                        "icon":"floppy",
                                        "key":"floppy",
                                        "value":"floppy"
                                }],
                                "propertyIcons":[{
                                        "icon":"room",
                                        "key":"roomPSqFt",
                                        "value":"1600 sq Ft"
                                },
                                {
                                        "icon":"beds",
                                        "key":"beds",
                                        "value":"2 beds"
                                },
                                {
                                        "icon":"bath",
                                        "key":"bath",
                                        "value":"1 bath"
                                },
                                {
                                        "icon":"garage",
                                        "key":"garage",
                                        "value":"1 garage"
                                }],
                                "propertyAddress":"Pole star city barajod Toll Plaza, Kanpur",
                            "name":"property 1",
                            "heading":"1",
                            "synopsis":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic ",
                            "priceRange":"120000-320000",
                            "minPrice":"120000",
                            "maxPrice":"320000",
                            "currency":"rupee",
                            "type":"independent House",
                            "location":"anekalgooglemaplink",
                            "locality":"Anekal",
                            "state":"Karnataka",
                            "propertyAction":"For Sale",
                            "pricePerSqft":"2400 Sq ft",
                            "roomDetails":[
                              {
                                "bedRoom":"2",
                                "bedRoomType":"onoe master/one normal",
                                "bathroom":"2",
                                "bathroomType":"one attached/one separate",
                                "flooring":"marble",
                                "studyRoom":"yes",
                                "beds":"3"
                              }],
                            "roomShortMetadata":{
                              "icons":{
                                "bedRoom":"2",
                                "bathRoom":"2",
                                "beds":"2"
                              }
                            }
                          }]
                      };
        }
        
        render() {
                return (
                        <div id="propertyPage" className="container-fluid">
                                
                                <PropertyImage />
                                
                                <div className="gallary">
                                        {/* Gallary componenet */}
                                        <div className ="propGallaryWrapperData">
                                                <div className="propGallaryData">
                                                        <div className="propName">{this.data.propertyDetails[0].propertyName}</div>
                                                        <div className="propAddress">{this.data.propertyDetails[0].propertyAddress}</div>
                                                        <div className="propType">{this.data.propertyDetails[0].propertyType}</div>
                                                        {/* <div className="propIcons">{this.data.propertyDetails[0].propertyIcons}</div> */}
                                                </div>
                                                <div className="propGallaryDataPrice">
                                                        <div className="propPrice">{this.data.propertyDetails[0].propertyPrice}</div>
                                                        <div className="propPerSqFt">{this.data.propertyDetails[0].propertyPerSqFt}</div>
                                                        {/* <div className="propIcons">{this.data.propertyDetails[0].propertyPriceIcons}</div> */}
                                                </div>
                                        </div>
                                </div>
                                
                                
                                <div className="propertyMetadata">
                                        {/* propertyMetadata componenet */}
                                        <PropertyTabs data = {this.data}/>
                                </div>
                                
                               
                                <EmiCalculator/>
                                <NewProperties />
                                
                        </div>
                )
        }
}
