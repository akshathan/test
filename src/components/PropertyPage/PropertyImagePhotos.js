export const PropertyImagephotos1 = [
    {
      src: "../../images/1.jpg",
      width: 4,
      height: 2.62
    }
];
export const PropertyImagephotos2 = [
    {
      src: "https://s3-alpha-sig.figma.com/img/8794/4bde/944d79a8cc7e982c054b9313f0c61397?Expires=1569801600&Signature=Jx2Y3cLijZLgKDFYWx3HmGP5uiEpnBL5mpuOsOH4-Dwx7bNVOECHVqkVKp6dzhrhZYvUKQomfCIhPCtFnEDDzr~4CQCVdWu~ZbTIy0CHf4voMPc5Uw8qZSWsw0JzegYUV~vXSeYfKsUHeY8fV7MyOX7LuDD9caSyBti-q-JLezWQTIvfDA8Ofdq-jMb12mPH200BsgBb2OiqQizsIJWrBlv8k33Qvis5TUIFHBP5PNgnWNYTieloE~U7scMJTpf57acdAbJQUkqng1Hxa6F5dkhJ2UnrNNW-OdfJ8GOnwKKAETPcdkQptH~ZghwGhsw3vNA8gK9ufetymXTJC6XfAQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
      width: 1,
      height: 1
    }
];

export const PropertyImagephotos3 = [
      {
        src: "https://s3-alpha-sig.figma.com/img/595b/bac8/6758860c86016857abda88a59ddcffcd?Expires=1569801600&Signature=Jks7shj9f0ukj7CS2PLhYJ6COssFHj1JMnjQeutEYqXfCf7liQd74cuNSssTrh0DwDmB1YnfqzfCiKDdKtuxp7FRkctGzeRLGJ-893oX18EJ1Bqo9E7i64fg814DPVfh3eAbahW7OGbmXoS0HY-Q0SDVq2C4bMcqE8A9Muzseo8lowS9i1kSuS7QuSmdtAu1Isn12p51Npp7-nedOtxyJPx2fQUQQ~--sIMOmjG-nxuzUkbHvIZT1pKyA1bB8X81E16XNXtMFmzgocrPOV2HoJEkJsgaRBrE488LNXgf3Daep5gbnzk0XZkMcC5301P5XQYr6Fmp0jr5w0~-QDsmow__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
        width: 1,
        height: 1
      }
    ];

export const PropertyImagephotos4 = [
      {
        src: "https://s3-alpha-sig.figma.com/img/4017/7573/846f066902493591b2b14de982351564?Expires=1569801600&Signature=Z8B5goZ8NrVYxZHcL~sm8wpzSH19HEGSxfSyJ1jp50INERVGUzNzV8AJRBoUPIZKRH1BtZJ8GRTsm1NeKKMXQbJCy~fOwuAI3AevTpuWdD9cYAbaSSF2MX-2~hXVdrg8JBRyyh1x52HzqT53tDTaIKWrxT99LltDrjso0a8U2IxX5KhBInNvNxNt65F2TL2TltvrkGmS4hjVyab~69jqPT8tkIEA~kJxEuYya8XnbwG2K~2fPwczYKZfxwDB-9-zoaTmOgrVGZ3IYU2OigfuH7TUunbkwIjC9XUe5fcDCztpyu4euKK82AmStwtMjWNJAEmV-jdDXYq08WTXVtYjtg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
        width: 1,
        height: 1
      }
    ];
export const PropertyImagephotos5 = [
      {
        src: "https://s3-alpha-sig.figma.com/img/464a/cb62/335ac389cc32eed7127f337b002148e6?Expires=1569801600&Signature=G~tW76ez0h9RO~yJUFLaWsVd1WSvi4kXBqpJu1OuvVd9bCMchEXRlGmXMfbqgpBIWoNpIcsa-1-yiHiWq4hiPE4wvHxfsxgSro05aTAKftJtoaGTS-EXw0jO54d4y~oBkZCZihv1AbODYhlps4YOvTWBim0BfiYJjxBmPa7f86sbME5aLFvxWGNIT08QR6AFNXr0xz8OvCf98Bt89YctHI9sw-wXBnec~UA9DhHDFHGrUA8QB3yhnaO3ao7xYDOvbSv5IofHkzrOzomiewqWoJWzVaqDO~rnN~qfvseCoKXxZuoHWOYsSXzNVq0y9ehZWshGYDdH-S-7PCzgw5DMLg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
        width: 1,
        height: 1
      },
      
    ];
export const PropertyImagephotos6 = [
        {
          src: "https://s3-alpha-sig.figma.com/img/4dc1/7f3a/cce4cb3f5b4b08853ef789acb2b9b742?Expires=1569801600&Signature=SrTNmwe-0lacDqthZXYPxA5rbVmnVqS642UC--B4pif9j2PZ1KBdTqpZma4KdF65vLO0mfb6dU-nms8xzyb9-4jnJhuBjrLt1NMl2EecXeohgpZNr6Vr5aVJ6ZAkWfRw-U~iR~1pozq49VfexsK-brXCL5X-yClaIaj-VyjDGjVRKrg9UCG75GslxX-6iUdh1BQ2SVQrpEV10zUpNw1P5W0h8rbyUWC7byfudmLH5HJ4D03CO24O9A2e3IEiEPRUpp617VYWPUvmMd5x73QoBtHNjqCqRkGPWp1ZMb3Jtbh2MgHZH2CxNbN9RZja-eg78ED2fnQRT2JCzo~hP6-F9Q__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
          width: 1,
          height: 1
        }
    ];
    