import React, { Component } from "react";
import {
  Form,
  Row,
  Col,
  Modal,
  Button,
  ButtonToolbar,
  ButtonGroup,
  Container,
  Image,
  Card,
  CardDeck
} from "react-bootstrap";

import { Link } from "react-router-dom";
import "../css/rigester.css";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import icon from "./../images/logoIcon.png";
import HomeIcon from "./../images/HomeIcon.png";
import avtar1 from "./../images/img_avatar.png";
import avtar2 from "./../images/img_avatar2.png";
import avtar3 from "./../images/img_avatar3.png";

export class Register extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { validated: false };
    this.state = { modalShow: false };
  }

  handleSubmit(event) {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    this.setState({ validated: true });
  }

  render() {
    const { validated } = this.state;
    let modalClose = () =>
      this.setState({
        modalShow1: false
      });

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Register</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            noValidate
            validated={validated}
            onSubmit={e => this.handleSubmit(e)}
          >
            <Form.Row>
              <Form.Group as={Col} md="6" sm="6">
                <Button type="submit" className="loginfacebook" size="md" block>
                  <i className="fab fa-facebook-f"></i> Facebook
                </Button>
              </Form.Group>
              <Form.Group as={Col} md="6" sm="6">
                <Button
                  variant="white gray border"
                  type="submit"
                  size="md"
                  block
                >
                  <i className="fab fa-google"></i> Google
                </Button>
              </Form.Group>
            </Form.Row>
            <hr className="hr-text" data-content=" or "></hr>
            <Form.Group as={Col} md="12" controlId="validationCustom01">
              <Form.Label>NAME * </Form.Label>
              <Form.Control
                className="controll"
                required
                type="text"
                placeholder="Name"
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>

            {/* <Form.Group as={Col} md="12" controlId="validationCustom02">
                <Form.Label>USER NAME *</Form.Label>
                <Form.Control
                  className="controll"
                  required
                  type="text"
                  placeholder="User Name"
                />
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              </Form.Group>
            */}

            <Form.Group as={Col} md="12" controlId="validationCustom03">
              <Form.Label>PHONE *</Form.Label>
              <InputGroup className="mb-3">
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Phone"
                  required
                />
                <Form.Control.Feedback type="invalid">
                  must be 10 numbers. Please provide a valid Phone number.
                </Form.Control.Feedback>
                <InputGroup.Append>
                  <Button
                    variant="outline-secondary"
                    className="btn btn-danger"
                    onClick={() => this.setState({ modalShow1: true })}
                  >
                    Sent OTP
                  </Button>
                </InputGroup.Append>
              </InputGroup>
              <OtpModal show={this.state.modalShow1} onHide={modalClose} />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom04">
              <Form.Label>EMAIL *</Form.Label>
              <Form.Control
                className="controll"
                type="email"
                placeholder="Email"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid Email id.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom05">
              <Form.Label>PASSWORD *</Form.Label>
              <Form.Control
                className="controll"
                type="password"
                placeholder="Password"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid password.
              </Form.Control.Feedback>
              <Form.Text className="red">
                Must be more than 8 Characters.
              </Form.Text>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom05">
              <Form.Label>LOCATION *</Form.Label>

              <Form.Control
                className="controll"
                type="Location"
                as="select"
                placeholder="location"
                required
              >
                <option>bangalore</option>
                <option>mysore</option>
                <option>chitradurga</option>
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                Please provide a valid password.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom06">
              <Form.Label>ADDRESS *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Address"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid Address.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom07">
              <Form.Label>About me *</Form.Label>
              <Form.Control
                className="controll"
                type="textarea"
                as="textarea"
                placeholder="About me "
                required
              />
              <Form.Control.Feedback type="invalid">
                write About me.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom08">
              <Form.Label>
                How can Beegru help with respect to your property needs?
              </Form.Label>
              <Form.Control
                className="controll"
                type="textarea"
                as="textarea"
                placeholder="Write a message "
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid message, atleast 10 words.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group>
              <Form.Check
                required
                label="I have read and agree to the Privacy Policy and 
                 Terms of use*"
                feedback="You must agree before submitting."
              />
            </Form.Group>

            <Form.Group>
              <Button type="submit" className="btn btn-danger" size="md" block>
                Register
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}
export default class Login extends Component {
  constructor(...args) {
    super(...args);

    this.state = { validated: false };
    this.state = { modalShow: false };
  }

  handleSubmit(event) {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    this.setState({ validated: true });
  }

  render() {
    const { validated } = this.state;
    let modalClose = () =>
      this.setState({
        modalShow2: false
      });

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container>
            <Row>
              <Col className="Loginmodaldiv1">
                <Image src={icon} fluid />
                <Image src={HomeIcon} fluid />

                <p>
                  Find the best matches for you Make the most of high seller
                  scores Experience a joyful journey
                </p>
              </Col>
              <Col className="Loginmodaldiv2">
                <Form
                  noValidate
                  validated={validated}
                  onSubmit={e => this.handleSubmit(e)}
                >
                  <Form.Group as={Col} md="12" controlId="validationCustom11">
                    <Form.Label>EMAIL</Form.Label>
                    <Form.Control
                      className="controll"
                      type="email"
                      placeholder="Email Id"
                      required
                    />
                    <Form.Control.Feedback type="invalid">
                      must be 8 characters strong. Please provide a valid email.
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group as={Col} md="12" controlId="validationCustom12">
                    <Form.Label>PASSWORD</Form.Label>
                    <Form.Control
                      className="controll"
                      type="password"
                      placeholder="Password"
                      required
                    />
                    <Form.Control.Feedback type="invalid">
                      must be 8 characters strong. Please provide a valid
                      Password.
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>
                      <Link href="#">Login with OTP</Link>
                    </Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} md="11">
                    <Button
                      type="submit"
                      className="btnFullwidth loginButton"
                      size="md"
                      block
                    >
                      Login
                    </Button>

                    <hr className="hr-text" data-content="or login via"></hr>

                    <Button
                      className="btnFullwidth loginfacebook"
                      size="md"
                      block
                    >
                      <i className="fab fa-facebook-f"></i> Facebook
                    </Button>

                    <Button
                      variant="white gray border"
                      className="btnFullwidth"
                      size="md"
                      block
                    >
                      <i className="fab fa-google"></i> Google
                    </Button>
                  </Form.Group>
                  <Form.Group className="login">
                    <p>
                      {" "}
                      Don't have an account? Please
                      {/* <Link href="#"> Register</Link> */}
                      <Link onClick={() => this.setState({ modalShow2: true })}>
                        Register
                      </Link>
                      <Register
                        show={this.state.modalShow2}
                        onHide={modalClose}
                      />
                    </p>

                    <p>
                      <Link href="#">Forgot your password?</Link>
                    </p>
                  </Form.Group>
                </Form>
              </Col>
            </Row>
          </Container>
        </Modal.Body>
      </Modal>
    );
  }
}

export class RegisterasAgent extends Component {
  constructor(...args) {
    super(...args);

    this.state = { validated: false };
  }

  handleSubmit(event) {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    this.setState({ validated: true });
  }

  render() {
    const { validated } = this.state;

    return (
      <Modal
        {...this.props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Register as agent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            noValidate
            validated={validated}
            onSubmit={e => this.handleSubmit(e)}
          >
            <Form.Group as={Col} md="12" controlId="validationCustom21">
              <Form.Label>NAME *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Name"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom22">
              <Form.Label>ADDRESS *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Address"
                required
              />
              <Form.Text className="red">
                Must be more than 9 Characters.
              </Form.Text>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom23">
              <Form.Label>SPECIALITY *</Form.Label>

              <Form.Control
                className="controll"
                type="speciality"
                as="select"
                placeholder="Speciality"
                required
              >
                <option>Speciality</option>
                <option>bangalore</option>
                <option>mysore</option>
                <option>chitradurga</option>
              </Form.Control>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom24">
              <Form.Label>LOCATION I AM EXPECT IN *</Form.Label>

              <Form.Control
                className="controll"
                type="Location"
                as="select"
                placeholder="Locations I am expert in"
                required
              >
                <option>Location i am expert in</option>
                <option>bangalore</option>
                <option>mysore</option>
                <option>chitradurga</option>
              </Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="11">
              <Button
                type="submit"
                className="btn btn-danger btnFullwidth"
                size="lg"
                block
              >
                Register As Agent
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class ListyourProperties extends Component {
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
  }

  render() {
    let modalClose = () =>
      this.setState({
        modalShow7: false,
        modalShow8: false,
        modalShow9: false,
        modalShow10: false,
        modalShow11: false,
        modalShow12: false,
        modalShow13: false,
        modalShow14: false,
        modalShow15: false,
        modalShow16: false,
        modalShow17: false
      });
    return (
      <Modal
        {...this.props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            List Your Properties
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasic">
              {/* <Form.Label>I want to</Form.Label> */}
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow7: true })}
                block
              >
                Residential for Sale
              </Button>
              <Residentialforsale
                show={this.state.modalShow7}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicEmail">
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow8: true })}
                block
              >
                Residential for Rent
              </Button>
              <Residentialforrent
                show={this.state.modalShow8}
                onHide={modalClose}
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasic">
              {/* <Form.Label>I want to</Form.Label> */}
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow9: true })}
                block
              >
                Commercial for Sale
              </Button>
              <Commercialforsale
                show={this.state.modalShow9}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicEmail">
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow10: true })}
                block
              >
                Commercial for Rent
              </Button>
              <Commercialforrent
                show={this.state.modalShow10}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasic">
              {/* <Form.Label>I want to</Form.Label> */}
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow11: true })}
                block
              >
                Industrial for Sale
              </Button>
              <Industrialforsale
                show={this.state.modalShow11}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicEmail">
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow12: true })}
                block
              >
                Industrial for Rent
              </Button>
              <Industrialforrent
                show={this.state.modalShow12}
                onHide={modalClose}
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasic">
              {/* <Form.Label>I want to</Form.Label> */}
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow13: true })}
                block
              >
                WareHousing for Sale
              </Button>
              <Warehousingforsale
                show={this.state.modalShow13}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicEmail">
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow14: true })}
                block
              >
                WareHousing for Rent
              </Button>
              <Warehousingforrent
                show={this.state.modalShow14}
                onHide={modalClose}
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasic">
              {/* <Form.Label>I want to</Form.Label> */}
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow15: true })}
                block
              >
                Agricultural for Sale
              </Button>
              <Agriculturalforsale
                show={this.state.modalShow15}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicEmail">
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow16: true })}
                block
              >
                Agricultural for Rent
              </Button>
              <Agriculturalforrent
                show={this.state.modalShow16}
                onHide={modalClose}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class ListyourProperty extends Component {
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
  }
  render() {
    let modalClose = () =>
      this.setState({
        modalShow4: false
      });
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            List your Property
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="postrequirement">
            <Form.Group as={Col} md="12" controlId="formBasicName">
              <Form.Label>NAME OF THE PROPERTY *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Name of the property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="">
              <Form.Label>I WANT TO *</Form.Label>
              <Form.Row>
                <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Sale"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Rent"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Find Joint Venture Partner-Investor"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>TYPE OF PROPERTY *</Form.Label>
              <Form.Row>
                <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Residential"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Commercial"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Warehousing"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Industrial"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Agricultural"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>LOCATION ADDRESS *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Location"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>LANDMARK *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Landmark"
                required
              />
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Landmark description"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>UPLOAD IMAGE *</Form.Label>
              <InputGroup>
                <Form.Control
                  placeholder="UPLOAD"
                  type="file"
                  multiple
                  required
                />
              </InputGroup>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>UPLOAD VIDEO </Form.Label>
              <InputGroup>
                <Form.Control
                  placeholder="UPLOAD"
                  type="file"
                  multiple
                  required
                />
              </InputGroup>
            </Form.Group>

            {/* <RadioGroup
    label="Meal Choice"
    onChange={this.handleMealChange}
    selectedValue={this.state.mealType}
>
    <Radio label="Soup" value="one" />
    <Radio label="Salad" value="two" />
    <Radio label="Sandwich" value="three" />
</RadioGroup> */}

            {/* <div className="custom-file">
                  <input type="file" className="custom-file-input" multiple />
                  <label
                    className="custom-file-label"
                    htmlFor="inputGroupFile01"
                  >
                    
                    Choose file
                  </label>
                  <i class="fa fa-cloud-upload"></i>
                </div> */}

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
                onClick={() => this.setState({ modalShow4: true })}
              >
                Post my Listing
              </Button>
              <ListyourProperties
                show={this.state.modalShow4}
                onHide={modalClose}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class PostyourRequirement extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Post your requirement
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p className="postform_text">
            If you have any specific requirements with respect of property
            buying, selling, renting or join ventures trasactions, send us a
            message or get n touch with us
          </p>
          <p className="postform_p">
            Email: mail@beegru.com
            <br />
            Phone: +91 88610 39999
          </p>
          {/* <Link to="/home">
            <p className="postform_link">or upload details</p>
          </Link> */}
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicName">
              <Form.Label>I WANT TO</Form.Label>
              <ButtonToolbar>
                <Button variant="outline-secondary" className="mlr-3">
                  SELL
                </Button>
                <Button variant="outline-secondary" className="mlr-3">
                  BUY
                </Button>
                <Button variant="outline-secondary" className="mlr-3">
                  RENT
                </Button>
                <Button variant="outline-secondary" className="mlr-3">
                  RENT-OUT
                </Button>
                <Button variant="outline-secondary" className="mlr-3">
                  JOINT-VENTURES
                </Button>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>PROPERTY LOCATION</Form.Label>
              <InputGroup className="mb-3 m1">
                <InputGroup.Prepend>
                  <Button variant="outline">
                    <i className="fa fa-search"></i>
                  </Button>
                </InputGroup.Prepend>
                <FormControl aria-describedby="basic-addon1" />
              </InputGroup>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>UPLOAD IMAGE *</Form.Label>
              <InputGroup>
                <Form.Control placeholder="UPLOAD" type="file" multiple />
              </InputGroup>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>COMMENTS</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="COMMENT IN BRIEF"
                required
              ></Form.Control>
            </Form.Group>

            {/* <input id="input-b3" name="input-b3[]" type="file" class="file" multiple 
    data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload..."/> */}
            {/* <video width="320" height="240" controls>
              <source src="movie.mp4" type="video/mp4" />
              <source src="movie.ogg" type="video/ogg" />
              Your browser does not support the video tag.
            </video> */}
            <Form.Group>
              <Form.Check
                className="bordernone"
                required
                label="I have read and agree to the Privacy Policy and 
                Terms of use
                *"
                feedback="You must agree before submitting."
              />
            </Form.Group>
            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post requirement
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Residentialforrent extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Residential for rent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12">
              <Form.Label>Type</Form.Label>

              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Apartment"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Land"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Independent House"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Plots"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Farm House"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Co-Living"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="other"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}></Form.Group>
              </Form.Row>
              <Form.Control
                className="controll"
                type="text"
                placeholder="other"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>if Co-Living Person</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Private Cabin"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Shared Work Space"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Length"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Breadth"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="A/c"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="GYM"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Laundry"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="WiFi"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Security"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Pool"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NRI"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Mansion"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenant Type</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Company"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Family"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Single"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pet Friendly</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Yes"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Agreement Period</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="3 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="6 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="12 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="2 years"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pereference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Non Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No perference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label=""
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenent habits preference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Drinking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Residentialforsale extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Residential for sale
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12">
              <Form.Label>Type</Form.Label>

              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Apartment"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Land"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Independent House"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Plots"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Farm House"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Co-Living"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="other"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}></Form.Group>
              </Form.Row>
              <Form.Control
                className="controll"
                type="text"
                placeholder="other"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="New"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Resale"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Under Constructions"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Ready to move in"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Built to Suit"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Bare Shell"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="furnished"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="semi-Furnished"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Un-Furnished"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>if Co-Living Person</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Private Cabin"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Shared Work Space"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Control
                    type="text"
                    label="Length"
                    className="controll"
                    placeholder="Length"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Control
                    type="text"
                    label="Breadth"
                    className="controll"
                    placeholder="Bredth"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="A/c"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="GYM"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Laundry"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="WiFi"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Security"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Pool"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Vicinity</Form.Label>
              <Form.Label>Type</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="type"
                required
              />
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NRI"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Mansion"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Agreement Period</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="3 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="6 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="12 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="2 years"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pereference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Non Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No perference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label=""
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenent habits preference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Drinking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              {/* <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar> */}
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Commercialforsale extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Commercial for Sale
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>if Co-Living Person</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Private Cabin"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Shared Work Space"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Length"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Breadth"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="A/c"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="GYM"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Laundry"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="WiFi"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Security"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Pool"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NRI"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Mansion"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Vicinity</Form.Label>
              <Form.Label>Type</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="type"
                required
              />
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Agreement Period</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="3 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="6 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="12 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="2 years"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pereference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Non Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No perference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label=""
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenent habits preference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Drinking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              {/* <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar> */}
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Commercialforrent extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Commercial for Rent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>if Co-Living Person</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Private Cabin"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Shared Work Space"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Length"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Breadth"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="A/c"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="GYM"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Laundry"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="WiFi"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Security"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Pool"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Agreement Period</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="3 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="6 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="12 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="2 years"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pereference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Non Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No perference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label=""
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenent habits preference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Drinking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Vicinity</Form.Label>
              <Form.Label>Type</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="type"
                required
              />
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Industrialforsale extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Industrial for Sale
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="A/c"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="GYM"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Laundry"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="WiFi"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Security"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Pool"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Agreement Period</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="3 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="6 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="12 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="2 years"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pereference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Non Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No perference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label=""
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenent habits preference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Drinking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Industrialforrent extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Industrial for Rent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="A/c"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="GYM"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Laundry"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="WiFi"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Security"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Pool"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Agreement Period</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="3 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="6 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="12 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="2 years"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pereference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Non Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No perference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label=""
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenent habits preference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Drinking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Warehousingforsale extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Warehousing for Sale
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="A/c"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="GYM"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Laundry"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="WiFi"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Security"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Pool"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Warehousingforrent extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Warehousing for Rent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="A/c"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="GYM"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Laundry"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="WiFi"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Security"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Pool"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="CCTV"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Agreement Period</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="3 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="6 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="12 months"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="2 years"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pereference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Non Veg"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No perference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label=""
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenent habits preference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Drinking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Smoking"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Agriculturalforsale extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Agricultural For Sale
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Kharad Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Water supply</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Canal"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Muncipality"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Borewell"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Soil type</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Red"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Black"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>
                Structure <span>super built up area</span>
              </Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land type</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Wet"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Dry"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Highlands"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Irrigation type</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Drip"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Sprinkler"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Canal"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Pipe"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Plantation</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Banana"
                required
              />
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Plantation description"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Agriculturalforrent extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Agricultural for rent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Kharad Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Water supply</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Canal"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Muncipality"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Borewell"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Soil type</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Red"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Black"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>
                Structure <span>super built up area</span>
              </Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land type</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Wet"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Dry"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Highlands"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Irrigation type</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Drip"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Sprinkler"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Canal"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Pipe"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Plantation</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Banana"
                required
              />
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Plantation description"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Loginmodal extends Component {
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
  }

  render() {
    let modalClose = () =>
      this.setState({
        modalShow1: false
      });

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter"></Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container>
            <Row>
              <Col className="Loginmodaldiv1">
                <Image src={icon} fluid />
                <Image src={HomeIcon} fluid />

                <p>
                  Find the best matches for you Make the most of high seller
                  scores Experience a joyful journey
                </p>
              </Col>
              <Col className="Loginmodaldiv2">
                <p>You are moments away from selling with Beegru</p>
                <InputGroup className="mb-3 m1">
                  <InputGroup.Prepend>
                    <Button variant="outline">
                      <i className="fa fa-search"></i>
                    </Button>
                  </InputGroup.Prepend>
                  <FormControl aria-describedby="basic-addon1" />
                </InputGroup>
                <p>Who are you?</p>
                <CardDeck>
                  <Card onClick={() => this.setState({ modalShow1: true })}>
                    <Card.Img variant="top" src={avtar1} thumbnail />
                    <Card.Body>
                      <Card.Text>Owner</Card.Text>
                    </Card.Body>
                  </Card>
                  <Card onClick={() => this.setState({ modalShow1: true })}>
                    <Card.Img variant="top" src={avtar2} thumbnail />
                    <Card.Body>
                      <Card.Text>Seller</Card.Text>
                    </Card.Body>
                  </Card>
                  <Card onClick={() => this.setState({ modalShow1: true })}>
                    <Card.Img variant="top" src={avtar3} thumbnail />
                    <Card.Body>
                      <Card.Text>Agent</Card.Text>
                    </Card.Body>
                  </Card>
                </CardDeck>
                <Login show={this.state.modalShow1} onHide={modalClose} />
              </Col>
            </Row>
          </Container>
        </Modal.Body>
      </Modal>
    );
  }
}

export class OtpModal extends Component {
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
  }

  render() {
    return (
      <Modal
        {...this.props}
        size="sm"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter"></Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12">
              <Form.Label>Enter OTP</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="****"
                required
              />
            </Form.Group>
            <Form.Row>
              <Form.Group as={Col} md="6" sm="6">
                <Button
                  className="btn btn-danger"
                  type="submit"
                  size="md"
                  block
                >
                  Verify
                </Button>
              </Form.Group>
              <Form.Group as={Col} md="6" sm="6">
                <Button
                  className="btn btn-danger"
                  type="submit"
                  size="md"
                  block
                >
                  Resent
                </Button>
              </Form.Group>
            </Form.Row>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class ListyourServices extends Component {
  constructor(...args) {
    super(...args);

    this.state = { validated: false };
  }

  handleSubmit(event) {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    this.setState({ validated: true });
  }

  render() {
    const { validated } = this.state;

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            List Your Services
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            noValidate
            validated={validated}
            onSubmit={e => this.handleSubmit(e)}
          >
            <Form.Group as={Col} md="12" controlId="validationCustom23">
              <Form.Label>SERVICE TYPE *</Form.Label>

              <Form.Control
                className="controll"
                type="speciality"
                as="select"
                placeholder="Speciality"
                required
              >
                <option>ARCHITECTURE</option>
                <option>Lawyer</option>
                <option>Interior</option>
                <option>Banker</option>
                <option>Other</option>
              </Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="validationCustom21">
              <Form.Label>SERVICE NAME *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Name"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>UPLOAD IMAGE *</Form.Label>
              <InputGroup>
                <Form.Control placeholder="UPLOAD" type="file" multiple />
              </InputGroup>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>ABOUT</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About your Services"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="validationCustom21">
              <Form.Label>Video URL</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="paste your video url"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description about your Services"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="validationCustom21">
              <Form.Label>Rating For Your Services</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="paste your video url"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="11">
              <Button
                type="submit"
                className="btn btn-danger btnFullwidth"
                size="lg"
                block
              >
                Post Your Services
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}
