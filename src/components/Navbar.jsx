import React, { Component } from "react";
import ScrollToTop from "react-scroll-up";
import "../css/Navbar.css";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Home from "./home.jsx";
import Bulletin from "./Bulletin.jsx";
import Services from "./services";
import SubServices from './Services/subServices';
import CompanyServices from './Services/companyServices';
import About from "./about.jsx";
import Header from "./Header.jsx";
import Beegrufooter from "./Footer.jsx";
import Login from "./Register.jsx";
import PropertyPage from "../components/PropertyPage/PropertyPage";
import PropertyList from "../components/PropertyPage/PropertyListPage";
import logo from "../images/logo.png"; // Tell Webpack this JS file uses this image
import { Navbar, Nav } from "react-bootstrap";

export default class BeegruNavbar extends Component {
  state = {};
  render() {
    return (
      <Router>
        <Header />
        <Beegrunav />

        <ScrollToTop showUnder={160}>
          <span>
            <i className="fas fa-arrow-up myBtn"></i>
          </span>
        </ScrollToTop>

        <Beegrufooter />
      </Router>
    );
  }
}

class Beegrunav extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }
  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render() {
    
    let modalClose = () =>
      this.setState({
        modalShow1: false
      });
    
    const collapsed = this.state.collapsed;
    const classOne = collapsed
      ? "collapse navbar-collapse"
      : "collapse navbar-collapse show";
    const classTwo = collapsed
      ? "navbar-toggler navbar-toggler-right collapsed"
      : "navbar-toggler navbar-toggler-right";
    return (
      <Router>
        {/* <nav className="navbar navbar-expand-lg navbar-inverse "  >
       
        <button
          className="navbar-toggler pull-left"
          type="button"
          data-toggle="collapse"
          data-target="#navbarTogglerDemo01"
          aria-controls="navbarTogglerDemo01"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="navbar-brand">
          <img src={logo} alt="Logo" />
        </div>

        <div className="collapse navbar-collapse " id="navbarTogglerDemo01">
          <ul className="navbar-nav">
            <li className="nav-item navElement"  data-toggle="collapse">
              <Link to="/home">Home </Link>
            </li>
            <li className="nav-item navElement">
              <Link to="/about">About</Link>
            </li>
            <li className="nav-item navElement">
              <Link to="/propertyPage">Property</Link>
            </li>
            <li className="nav-item navElement">
              <Link to="/propertyList">Property List</Link>
            </li>
            <li className="nav-item navElement dropdown">
              <Link to="/Services">SERVICES</Link> */}
        {/* <button className="dropbtn">
                                      <Link to="/">SERVICES</Link> 
                                    </button>
                                    <div className="dropdown-content square">
                                       <Row className="rowline">
                                         <Col lg={4} md={4} sm={4} xs={4}>
                                         <div
                                         image={require('./../images/3.jpg')}
                                         ></div>
                                         </Col>
                                         <Col lg={8}  md={8} sm={8} xs={8}>
                                          
                                           <div className="dropdown-contentheading">Tanants</div>
                                           <span>Rent without a deposit</span>
                                         </Col>
                                       </Row>
                                       <Row className="rowline">
                                         <Col lg={4} md={4} sm={4} xs={4}>
                                        
                                         </Col>
                                         <Col lg={8}  md={8} sm={8} xs={8}>
                                          
                                           <div className="dropdown-contentheading">Landlords</div>
                                           <span>Get the best protection</span>
                                         </Col>
                                       </Row>
                                       <Row className="rowline">
                                         <Col lg={4} md={4} sm={4} xs={4}>
                                         
                                         </Col>
                                         <Col lg={8}  md={8} sm={8} xs={8}>
                                          
                                           <div className="dropdown-contentheading">Partners</div>
                                           <span>Let your properties faster.</span>
                                         </Col>
                                       </Row>
                                    </div>
                                  */}
        {/* </li>
            <li className="nav-item navElement">
              <Link to="/bulletin">bulletin</Link>
            </li>
            <li className="nav-item navElement">
              <Link to="" onClick={() => this.setState({ modalShow17: true })}>LOGIN</Link>
            </li>
            <Loginmodal show={this.state.modalShow17} onHide={modalClose} />
          </ul>
        </div>
      </nav>
       */}

        <Navbar collapseOnSelect expand="lg" variant="inverse">
          <Navbar.Brand href="#home">
            {" "}
            <img src={logo} alt="Logo" />
          </Navbar.Brand>
          <Navbar.Toggle
            aria-controls="responsive-navbar-nav"
            className="pull-left"
          />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ml-auto nav1 ">
              <Nav.Link eventKey="1" as={Link} to="/home">
                HomePage
              </Nav.Link>
              <Nav.Link eventKey="2" as={Link} to="/propertyPage">
                Property
              </Nav.Link>
              <Nav.Link eventKey="3" as={Link} to="/services">
                SERVICES
              </Nav.Link>
              <Nav.Link eventKey="4" as={Link} to="/propertyList">
                Property List
              </Nav.Link>
              <Nav.Link eventKey="5" as={Link} to="/about">
                About Us
              </Nav.Link>
              <Nav.Link eventKey="6" as={Link} to="/bulletin">
                Bulletin
              </Nav.Link>
            
            <Nav.Link eventKey="7"  to=""
             
              onClick={() => this.setState({ modalShow1: true })}>
                Login
              </Nav.Link>
              <Login show={this.state.modalShow1} onHide={modalClose} />
            </Nav>
          </Navbar.Collapse>
          
          {/* <Nav className="nav2">
            <Nav.Link
              to=""
              onClick={() => this.setState({ modalShow17: true })}
            >
              LOGIN
            </Nav.Link>
            <Login show={this.state.modalShow17} onHide={modalClose} />
          </Nav> */}
        </Navbar>

        <Route exact path="/" component={Home} />
        <Route exact path="/home" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/propertyPage" component={PropertyPage} />
        <Route path="/propertyList" component={PropertyList} />

        <Route path="/services" component={Services} />
        <Route path="/bulletin" component={Bulletin} />
        <Route path="/subServices" component={SubServices} />
        <Route path="/companyServices" component={CompanyServices} />
      </Router>
    );
  }
}
