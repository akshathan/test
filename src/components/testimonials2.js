import React, { useState} from "react";
import "bootstrap/dist/css/bootstrap.css";
import Card from 'react-bootstrap/Card';
import CardDeck from 'react-bootstrap/CardDeck';
import Modal from 'react-bootstrap/Modal';
import "react-multi-carousel/lib/styles.css";
import '../css/testimonials2.css';
import ReactCardCarousel from "react-card-carousel";
import { Container,Row,Col } from "react-bootstrap";

export default class HappyTestimonials extends React.Component {
      state = {  }
      render() { 
       
          return (  
            <div className="testimonialcontainer">
              
                <Row >
                    <Col md="6">
                      
                      <div className="testimonialheading">
                        <p> Happy People’s</p>
                           Testimonials
                        
                      </div>
                    </Col>

                    <Col md="6">
                    <ReactCardCarousel autoplay={true} autoplay_speed={2500}>
                      <div>
                          <Card>
                              <Card.Header>
                                <Row>
                                  <Col xs="8">
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>

                                  </Col>
                                  <Col xs="4" className="date">
                                     Sept 14th
                                  </Col>
                                </Row>
                              </Card.Header>

                              <Card.Body>
                                <Card.Title>Everthing was amazing</Card.Title>
    
                                <Card.Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                   Nullam turpis arcu, pretium ut eros.
                                   Sed et neque nec leo maximus aliquet sit amet vel tellus. Proin sit amet consequat ipsum. Aliquam laoreet pharetra eros, a placerat
                                     eros convallis quis. Pellentesque tristique eleifend sapien vitae imperdiet.
                          
                                   Vestibulum scelerisque venenatis nisi, ac </Card.Text>
                                   
                              </Card.Body>
                              <Card.Footer>
                                <Row>
                                  <Col xs="2">
                                    
                                    <Card.Img src="images/customer1.png" className="testimonial-image" />
                                  </Col>  
                                  <Col xs="8">
                                     <p>Maja Christiansen</p>
                                     <p1>Shriram Value Homes At Divin..</p1>
                                  </Col>
                                  <Col xs="2">
                                    <i className='fa fa-share-alt' ></i>
                                  </Col>


                                </Row>
                             
                               
                              </Card.Footer>
                              
                          </Card>
                        </div>
                        <div>
                            <Testimonialcard />
                        </div>
                      
                        <div>
                            <Testimonialcard />
                        </div>
                      
                        
                      
                        </ReactCardCarousel>
                    </Col>
                   
            </Row>
          </div>
          );
      }
  }
 


 function Testimonialcard(){
  const [show, setShow] = useState(false);
   return(
        <CardDeck>
          <Card onClick={() => setShow(true)}>
            <Card.Header>
              <Row>
                <Col xs="8">
                  <i className='fa fa-star' ></i>
                  <i className='fa fa-star' ></i>
                  <i className='fa fa-star' ></i>
                  <i className='fa fa-star' ></i>
                  <i className='fa fa-star' ></i>

                </Col>
                <Col xs="4" className="date">
                  May 10th
                </Col>
               </Row>
            </Card.Header>
            <Card.Body>
              <Card.Title>Awesome Experience!</Card.Title>
              <Card.Text>We enjoyed our Israel trip immensely. The hotels were wonderful…we really
                  enjoyed Meir, our tour guide. He was so knowledgeable. His experience as a soldier
                  and commander in the ‘67 war and the Yom Kippur War made us feel like we were seeing 
                  the country through the eyes of a 
                  
                  person that was instrumental in its formation and growth. A true Sabra! </Card.Text>
            </Card.Body>
            <Card.Footer>
              <Row>
                <Col xs="2">
                  <img src="../images/customer1.png" className="testimonial-image"></img>
                </Col>  
                <Col xs="8">
                  <p>Sofie Olsen</p>
                  <p1>Shriram Value Homes At Divin</p1>
                </Col>
                <Col xs="2">
                  <i className='fa fa-share-alt' ></i>
                </Col>
              </Row>
            </Card.Footer>
          </Card>
          <Modal 
          show={show}
          size="lg"
          onHide={() => setShow(false)} 
          >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
                    
                         Happy People’s
                       Testimonials
                        
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
             <Col md="7">
               <img src="../images/customer1.png"></img>
             </Col>

             <Col md="5" >
             <Row>
                                  <Col xs="8">
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>

                                  </Col>
                                  <Col xs="4" className="date">
                                     Sept 14th
                                  </Col>
                                </Row>
                                <Row>
                                <Card.Body>
              <Card.Title>Awesome Experience!</Card.Title>
              <Card.Text>We enjoyed our Israel trip immensely. The hotels were wonderful…we really
                  enjoyed Meir, our tour guide. He was so knowledgeable. His experience as a soldier
                  and commander in the ‘67 war and the Yom Kippur War made us feel like we were seeing 
                  the country through the eyes of a 
                  person that was instrumental in its formation and growth. A true Sabra! </Card.Text>
            </Card.Body>
                                </Row>
                                <Row>
                                  <Col xs="2">
                                    <img src="../images/customer1.png" className="testimonial-image"></img>
                                  </Col>  
                                  <Col xs="8">
                                     <p>Maja Christiansen</p>
                                     <p1>Shriram Value Homes At Divin..</p1>
                                  </Col>
                                  <Col xs="2">
                                    <i className='fa fa-share-alt' ></i>
                                  </Col>


                                </Row>
             </Col>

          </Row>

        </Modal.Body>
      </Modal>
      
        </CardDeck>
       
        
   );

 }

 