import React, { Component, useState, useCallback } from 'react'
import { BrowserRouter as Router } from "react-router-dom";
import { Container} from 'react-bootstrap';
import "../../css/Blog.css";
import Gallery from "react-photo-gallery";
import Carousel, { Modal, ModalGateway } from "react-images";
import { BlogGallery } from "./BlogGallery";



export default class MainBlog extends Component {
    state = {}
    render() {
        return (

            <Router>
                <Container className="BlogPageWrapper">
                    <div className="heading">
                        <ul>
                            <li>Blog</li>

                        </ul>
                    </div>
                    <BlogGalleryView />
                </Container>

            </Router >

        );
    }
}

function BlogGalleryView() {
    const [currentImage, setCurrentImage] = useState(0);
    const [viewerIsOpen, setViewerIsOpen] = useState(false);

    const openLightbox = useCallback((event, { photo, index }) => {
        setCurrentImage(index);
        setViewerIsOpen(true);
    }, []);

    const closeLightbox = () => {
        setCurrentImage(0);
        setViewerIsOpen(false);
    };

    return (
        <div>

            <Gallery photos={BlogGallery} onClick={openLightbox} />
            <ModalGateway>
                {viewerIsOpen ? (
                    <Modal onClose={closeLightbox}>
                        <Carousel
                            currentIndex={currentImage}
                            views={BlogGallery.map(x => ({
                                ...x,
                                srcset: x.srcSet,
                                caption: x.title
                            }))}
                        />
                    </Modal>
                ) : null}
            </ModalGateway>
        </div>
    );
}
