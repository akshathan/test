import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Button, Row, Col, Container } from "react-bootstrap";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import "../css/HomeSearch.css";
// import data from "../SampleData/data.json";
// import iconsConfig from "../components/Utililities/IconsConfig";
// import SearchFilterIcon from "../components/Search/searchFilterIcon";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
// import Dropdown from "react-bootstrap/Dropdown";
// import DropdownButton from "react-bootstrap/DropdownButton";

class HomeSearch extends React.Component {
  // constructor(props){
  //     super(props);
  //     }
  //     componentDidMount(){
  //         document.getElementById("formBasicSearch").addEventListener("focus",function(){
  //             document.getElementsByClassName("searchFilters")[0].style.display = "block";
  //         });
  //     }
  render() {
    return (
      <Router>
        <div className="Homefrantpage">
          <Row className="buttonContainer nomargin">
            <Col>{<HomepageButtons />}</Col>
          </Row>

          <Container>
            {/* <Form className=" searchform">
                                  <i class="fa fa-home searchIcon"></i> 
                                  <Form.Group controlId="formBasicSearch" className="fromgrp">
                                  
                                    <Form.Control className="SearchBar" type="text" placeholder="Enter Place,Builder, Project" />
                                    {/* <Form.Control className="SearchBar searchinput" type="text" placeholder="Start Searching" /> */}
            {/* <i class="fas fa-crosshairs searchIcon"></i>  
                                    </Form.Group>
                                    <Button className="SearchButton" type="submit"> SEARCH </Button>
                                </Form> */}
            {/* <div class="searchContainer1">
              <i class="fa fa-home searchIcon1"></i>
              <input
                class="searchBox1"
                type="search"
                name="search"
                placeholder="Enter Place,Builder, Project..."
              />
              <i class="fa fa-crosshairs searchIcon1"></i>
              <input type="submit" value="Search" class="SearchButton" />
            </div> */}

            <InputGroup size="lg" className="mb-3">
              {/* <InputGroup.Prepend>
                <InputGroup.Text><i class="fa fa-home"></i></InputGroup.Text>
                <i class="fa fa-search"></i>
              </InputGroup.Prepend> */}
              <FormControl
                type="search"
                name="search"
                placeholder="Enter Place,Builder, Project..."
                aria-label="Enter Place,Builder, Project..."
                aria-describedby="basic-addon2"
              />
              <InputGroup.Append>
                <InputGroup.Text>
                  <i className="fa fa-crosshairs"></i>
                </InputGroup.Text>
                <Button variant="danger" value="search" className="searchButton1">
                  search
                </Button>
              </InputGroup.Append>
            </InputGroup>

            {/* <Row className="searchFilters">  
                                  <Col className="col-lg-6 col-md-6 col-sm-12 col-xs-12 col">
                                    <div data-role="main" class="ui-content">
                                      <form method="post" action="/action_page_post.php">
                                        <div data-role="rangeslider">
                                           <label for="price-min">Price:</label>
                                           <input type="range" name="price-min" id="price-min" value="200" min="0" max="1000"></input>
                                           <label for="price-max">Price:</label>
                                           <input type="range" name="price-max" id="price-max" value="800" min="0" max="1000"></input>
                                        </div>
                                        <input type="submit" data-inline="true" value="Submit"></input>
                                        <p>The range slider can be useful for allowing users to select a specific price range when browsing products.</p>
                                      </form>
                                    </div>
                                   </Col>
                                    <Col className="col-lg-6 searchButtons">
                                        <div className="headingType">
                                            Property Type
                                        </div>
                                        {<SearchFilterIcon />}
                                    </Col>
                                </Row> */}
            {/* design is same as square yard website <InputGroup size="lg">
              <DropdownButton
                as={InputGroup.Prepend}
                variant="outline-secondary"
                title="All Cities"
                id="input-group-dropdown-1"
              >
                <Dropdown.Item href="#">Action</Dropdown.Item>
                <Dropdown.Item href="#">Another action</Dropdown.Item>
                <Dropdown.Item href="#">Something else here</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item href="#">Separated link</Dropdown.Item>
              </DropdownButton>
              <FormControl
                placeholder="Search by Location"
                aria-label="Search by Location"
                aria-describedby="basic-addon2"
              />

              <DropdownButton
                as={InputGroup.Append}
                variant="outline-secondary"
                title="Budget"
                id="input-group-dropdown-2"
              >
                <Dropdown.Item href="#">Action</Dropdown.Item>
                <Dropdown.Item href="#">Another action</Dropdown.Item>
                <Dropdown.Item href="#">Something else here</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item href="#">Separated link</Dropdown.Item>
              </DropdownButton>
              <DropdownButton
                as={InputGroup.Append}
                variant="outline-secondary"
                title="Type"
                id="input-group-dropdown-2"
              >
                <Dropdown.Item href="#">Action</Dropdown.Item>
                <Dropdown.Item href="#">Another action</Dropdown.Item>
                <Dropdown.Item href="#">Something else here</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item href="#">Separated link</Dropdown.Item>
              </DropdownButton>
            </InputGroup> */}
          </Container>
        </div>
      </Router>
    );
  }
}

export default HomeSearch;

// function Search(){

//     return(

//         <Form className="searchform">
//             <Form.Group controlId="formBasicSearch" className="fromgrp">
//                 <Form.Control className="SearchBar searchinput" type="text" placeholder="Start Searching" />
//             </Form.Group>
//             <Button className="SearchButton" type="submit"> SEARCH </Button>

//         </Form>

//     );
// }

function HomepageButtons() {
  return (
    <ButtonGroup className="HomepageButtons ">
      <button className="BuyButton buttonHomepage">Buy</button>
      {/* <div className="divider"></div> */}
      <button className="RentButton buttonHomepage">Rent</button>
      <button className="LeaseButton buttonHomepage">Lease</button>
      <button className="InvestButton buttonHomepage">Invest</button>
      <button className="InvestButton buttonHomepage">Joint Venture</button>
    </ButtonGroup>
  );
}
